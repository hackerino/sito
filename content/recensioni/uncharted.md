+++
title = "Uncharted"
date = 2022-02-20
[taxonomies]
recensione = ["film"]
[extra]
voto = 2
+++

Non so nemmeno io perché ho deciso di vederlo, forse perché ho visto troppe interviste di Tom Holland (almeno con Rovazzi e Matano), forse perché generalmente mi piacciono i film di tipo storico/avventuroso. Il risultato peró è mediocre. Il formato è di quelli già visti e rivisti, come se io per qualche strano motivo mi fossi ritrovato regista alla prima esperienza e avessi seguito un corso di storytelling. Perché è questa l'impressione che ne ho: una trama amatoriale. Tutto ciò che ci gira intorno è meravigliosamente bello, ma resta una Ferrari con il motore di un Panda.
