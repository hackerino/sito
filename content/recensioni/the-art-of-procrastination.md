+++
title = "The Art of Procrastination - John Perry"
date = 2021-12-14
[taxonomies]
recensione = ["libri"]
[extra]
voto = 3
+++

Libretto godevole, non lascia molto materiale pratico, ma le battute taglienti dell'autore sono puntuali e ben studiate, e mi hanno fatto ridere da solo come un cretino più di un paio di volte.

Presenta un paio di idee interessanti, come quella della scrivania circolare, e poi per il resto cerca di convincere il lettore a non prendersela e a non essere troppo duro con se stesso. Ricorda a tratti un libro di self-help, l'autore ironizza anche su questo, probabilmente dopo essersene reso conto e cercando di correggere il tiro. 


Si può trovare gratis in prestito su [archive.org](https://archive.org/details/artofprocrastina0000perr). 
