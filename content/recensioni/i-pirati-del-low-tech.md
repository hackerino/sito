+++
title = "I pirati del low-tech"
date = 2020-10-08
[taxonomies]
recensione = ["videoserie"]
[extra]
voto = 4
+++

Me la sono sparata in pochi giorni, endovena. Unisce i temi della tecnologia e dell'ambientalismo, una combinazione micidiale per la mia dopamina, che ha dei picchi non da poco. Un voto in meno soltanto perché è forse la serie è forse troppo generalista e troppo poco pratica, come ho dolorosamente notato nella puntata intitolata su di un computer low-tech, che si è rivelato un Raspberry Pi.

Si può trovare gratis su [Arte.tv](https://www.arte.tv/it/videos/RC-016865/pirati-del-low-tech/) (occhio che scade). 
