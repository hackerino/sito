+++
title = "Come parsificare le opzioni in C con getopt() e getopt_long()"
date = 2021-02-11
[taxonomies]
tags = [ "c", "programmazione"]
categorie = [ "Programmazione" ]
+++

Spesso quando si programma uno strumentino a linea di comando si ha la necessità di **accettare delle opzioni**. Siccome è alquanto inefficiente e decisamente votato all'errore riscrivere lo stesso codice, per quanto semplice possa essere, oggi vediamo **un paio di funzioni C** molto comode che consentono di parsificare le opzioni, con alcuni gradi di possibilità; parlo di **`getopt()`** e **`getopt_long()`**, iniziamo.

## Le funzioni
La prima funzione, `getopt()`, consente di parsificare **opzioni brevi** (singoli caratteri, es. `-o`) e di **gestire argomenti** (ad esempio `-o programma`). Questa è la [specifica POSIX](https://pubs.opengroup.org/onlinepubs/9699919799/nframe.html), alla quale conviene attenersi se si punta alla massima portabilità. Esistono estensioni di questa funzione, come l'[estensione GNU](https://www.gnu.org/software/libc/manual/html_node/Using-Getopt.html), che permette di avere argomenti facoltativi. È necessario includere la libreria `#include <unistd.h>`.

`getopt_long()` come dice il nome permette di avere, oltre alle canoniche opzioni brevi anche le **opzioni lunghe**, ovvero quelle precedute da due trattini come in `--version`. Bisogna includere `#include <getopt.h>`, essendo un'[estensione GNU](https://linux.die.net/man/3/getopt_long) e quindi assente dalla libreria POSIX. 

## Comunanze
L'idea è comune e semplice: si passano alla funzione `getopt*()` i parametri `argc` e `argv` del main, più una/due strutture dati che specificano quali opzioni sono accettabili e come.

A ogni chiamata la funzione **restituisce il carattere che identifica l'opzione**, che può essere impostato sempre tramite la stessa struttura dati. L'idea è quella di avere uno **switch sul valore di ritorno** che smista tutte le possibili opzioni e permette di eseguire operazioni differenziate o di configurare alcune variabili senza allungare il brodo.

In aggiunta `getopt_long()` permette anche di **impostare direttamente un flag** senza doverlo settare esplicitamente. In questo caso viene ritornato `0`. 

In presenza di un'**opzione sconosciuta** il programma ritorna `'?'`.

Dopo aver elaborato tutte le opzioni viene ritornato `-1`.

### Variabili globali
Ci sono alcune variabili globali che vengono usate da entrambe le funzioni.

La prima è `optind`, che contiene l'**indice di argv** del prossimo elemento da elaborare, e può essere utile per ottenere l'indice del primo elemento di `argv` quando `getopt*()` non ha più opzioni da processare (ad esempio dopo `--`, che interrompe la lettura). 

Poi c'è `opterr`, che serve per **disabilitare** (a 0) **la stampa dell'errore** se si riceve un'opzione sconosciuta o un argomento mancante. In ogni caso quando avviene un errore viene ritornato `'?'`, che quindi può essere usato per gestire l'errore manualmente. 

`optopt` contiene il carattere dell'opzione che ha scatenato l'errore.

Infine `optarg` contiene il **puntatore all'argomento** dell'opzione che si sta trattando.

### Configurazione
Sia `getopt()` che `getopt_long()` accettano una stringa per le opzioni brevi, la seconda in più accetta un vettore di strutture per le opzioni lunghe.

#### getopt()
```c
int getopt (int argc, char *const *argv, const char *options)
```
La stringa che descrive le opzioni accettate è composta dal **carattere dell'opzione** (es. `o` per `-o`), seguito da `:` se l'opzione richiede un **argomento**. Per accettare **argomenti facoltativi** il carattere è seguito da `::`. Il carattere restituito dalla funzione è il carattere dell'opzione.

#### getopt_long()
```c
int getopt_long (int argc, char *const *argv, const char *shortopts, const struct option *longopts, int *indexptr)
```
Per le opzioni brevi (shortopts) si usa la **stessa stringa di `getopt()`**.

Per quelle lunghe (longopts) il vettore di strutture passato è del tipo:
```c
//name		has_arg			flag	val
//const char*	int			int*	int
{"add",		no_argument,       	0,	'a'},
{"append",  	no_argument,       	0,	'b'},
{"delete",  	required_argument, 	0,	'd'},
{"create",  	required_argument, 	0,	'c'},
{"file",    	required_argument, 	0,	'f'},
```

Oppure:
```c
{"verbose",	no_argument,	&verbose_flag,	1},
{"brief",	no_argument,	&verbose_flag,	0},
```

Come vedete, la differenza risiede in **flag**. Nel primo caso è a **0** (`NULL`), quindi significa che getopt_long() deve ritornare il contenuto di `val`, mentre quando è impostato a un **puntatore a intero**, esso diventa il destinatario del valore contenuto in `val`. In questo secondo caso **ritorna 0**. 

È evidente come nel primo esempio sia semplice avere **l'opzione breve e lunga identificate dallo stesso carattere**, riconducendo l'opzione breve e lunga allo stesso significato e alla stessa gestione nello switch. Basta infatti usare lo stesso carattere nel campo `val` di longopts e nella stringa `shortopts`. 

L'ultimo parametro di cui non ho ancora parlato, `indexptr`, è passato per referenza e viene impostato all'**indice del vettore longopts** che si sta trattando al momento.
