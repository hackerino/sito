+++
title = "wget: comando e opzioni utili"
date = 2020-11-05
[taxonomies]
tags = [ "internet", "backup"]
categorie = [ "Software" ]
+++

**wget** è uno dei migliori comandi che viene con Linux per scaricare file da internet, e può essere usato anche per clonare interi siti internet. In questo articolo il comando sarà presentato e ne verranno mostrati all'opera gli esempi più utili.

## Infobriciole 
Il comando supporta i protocolli HTTP(S) e FTP(S). In origine era chiamato Geturl, ma poi il nome fu cambiato dopo che gli sviluppatori scoprirono dell'esistenza di un programma sull'Amiga con lo stesso nome.

## Usi
### Scaricare un file
L'uso più banale è quello di scaricare un singolo file dato un URL. Si esegue così:
```sh
$ wget URL
```
### Scaricare più file
Si possono anche scaricare file da un elenco di URL deciso, che è scritto in un file di testo un URL per riga. Si esegue così:
```sh
$ wget -i lista.txt
```
### Scaricare ricorsivamente un sito (clonare un sito)
Con le opzioni giuste il comando può servire anche per ottenere per l'uso locale una sezione o addirittura l'intero sito, a seconda della profondità dell'URL specificato. Sono necessarie alcune opzioni: `-r` abilita il recupero ricorsivo, `-k` converte i link per un uso in locale, `-p` scarica i file necessari per la corretta resa della pagina. Con `-np` si può poi evitare di risalire nell'albero delle cartelle, scaricando soltanto i file che si trovano più in basso. Si esegue così:
```sh
$ wget -r -k -p -np URL
```

### Scaricare tutti i file di un certo formato
Questo comando può essere utile per scaricare da quei siti che dispongono di materiale collegato a pagine HTML, ma magari si è interessati solo ai PDF. Con `-A` si può definire il suffisso o il pattern che va bene scaricare. Può essere abbinato allo scaricamento ricorsivo per scaricare ad esempio tutti i file di un formato in un sottoalbero di cartelle (o anche in tutto il sito). Si esegue così:

```sh
$ wget -r -np -A "*.pdf" URL
```

### Escludere tutti i file di un certo formato
Al contrario a volte si potrebbe voler scaricare tutti i file tranne quelli di un certo formato, ad esempio HTML quando si vogliono scaricare i file da una cartella senza la pagina `index.html`. Questo è ciò che fa `-R`, che accetta una stringa analoga a quella precedente. Si esegue così: 

```sh
$ wget -r -np -R "*.html" URL
```

### Limitare la banda usata 
Se purtroppo non avete la [connessione della NASA](https://www.rvcj.com/nasas-internet-speed-91-gbps-13000-times-faster-know-truth-behind/), ma vi ritrovate ugualmente a dover condividere la vostra banda con qualcuno, limitare la banda usata può essere fondamentale. Questo con wget è abbastanza facile da fare, infatti si può aggiungere semplicemente `--limit-rate`. È specialmente utile quando si scarica una lista di grandi file, come ad esempio dei video super-mega-utili, ma non volete farvi menare dai vostri conviventi. Si esegue così:

```sh
# Esempio con lista e limite a 200 KB/s
$ wget -i lista.txt --limit-rate=200k
```

### Superare barriere d'accesso
Se fosse facile così anche nella vita vera. Per scaricare da connessioni protette con utente e parola d'ordine, di solito FTP ma raramente anche HTTP, basta specificarli con le opzioni --user e --password, valide sia per FTP che per HTTP. Si esegue così:

```sh
$ wget --user=pierino --password=piumelomenopiuvengomeno URL
```
