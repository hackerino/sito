+++
title = "Come lanciare i videogiochi Epic Games su Linux"
date = 2020-09-03
[taxonomies]
tags = [ "videogiochi", "linux"]
categorie = [ "Software" ]
+++

Epic Games sta pompando giochi gratuiti uno dopo l'altro, al ritmo assurdo di due a settimana. Per noi linari è un peccato, perché possiamo riscattarli dal sito, ma l'Epic Launcher non è di certo pensato per Linux. Per fortuna ci viene in soccorso derrod con il suo [Legendary](https://github.com/derrod/legendary), un'alternativa all'Epic Launcher per scaricare e lanciare i giochi Epic. In questa guida vediamo come installarlo, i suoi comandi e come integrarlo in Lutris.

## Installazione
Legendary è scritto in Python (3.8+), per cui è una buona idea scaricarlo se mancasse.
Attualmente è disponibile nei repository Arch (AUR), Fedora, openSUSE e Gentoo.

Se non fosse a disposizione nei vostri repository, il modo più semplice per installarlo è usare:

```
$ pip install legendary-gl
```
Per un elenco completo delle alternative d'installazione vedete il README.md su GitHub.

Per lanciare i giochi in Linux serve poi `wine`, che dovrebbe essere abbastanza onnipresente nei soliti repository. 

## Uso
Prima di fare tutto è necessario accedere. Per farlo si usa:
```
$ legendary auth
```
Questo aprirà una scheda nel browser per accedere al sito Epic Games. Una volta entrati, si vedrà un oggetto JSON, basta copiare la stringa dell'attributo `sid` e incollarlo nel terminale.

Per elecare i giochi a disposizione nell'account:
```
$ legendary list-games
```

Questo comando restituirà una lista simile alla seguente:
```
Available games:
 * Enter the Gungeon (App name: Garlic | Version: 2.1.9a)
 * God's Trigger (App name: 9bc4423d873845739cc99ab69b6bfbe9 | Version: 1.2.58760.3)

Total: 2
```

Prestate attenzione al parametro `App name`, che sarà il nome da usare per installare e lanciare il gioco.

Per installare un gioco (ad esempio *Enter the Gungeon*):
```
$ legendary install Garlic
```

Per eseguire un gioco (sempre lo stesso):
```
$ legendary launch Garlic
```

### Integrazione in Lutris
Non essendo (ancora) disponibile una GUI, Lutris ci viene in soccorso.

Un esempio per tutti, con il solito *Enter the Gungeon*.

Le sezioni da impostare sono:
* sotto Game info:
  * name (possibilmente giusto per ottenere una copertina garna)
  * runner -> Linux
* sotto Game options:
  * executable -> legendary
  * arguments -> launch Garlic

Fine, il gioco è aggiunto nella libreria Lutris, pronto per essere eseguito.
