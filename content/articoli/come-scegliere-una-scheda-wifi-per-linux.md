+++
title = "Come scegliere una scheda WiFi per Linux"
date = 2020-11-13
[taxonomies]
tags = [ "wifi", "linux"]
categorie = [ "Linux" ]
+++

Nonostante gli enormi progressi nel supporto per i driver Linux, c'è ancora un tipo particolare di driver che ha un supporto abbastanza scadente: i driver WiFi. In questo articolo riassumerò le cose da considerare per scegliere una scheda WiFi che funzioni con Linux. Cominciamo.

## Idee generali
Quello che fa la differenza in una scheda WiFi è il chipset della scheda. Da esso infatti dipende il supporto dei driver per la scheda. Modelli e marche diverse possono usare lo stesso chipset e spesso lo fanno, quindi non è sufficiente questo livello di indagine. In più la maggior parte dei prodotti non lo cita nemmeno nelle specifiche tecniche, per cui può anche risultare difficile proseguire l'indagine per controllare se è supportato dal kernel.

Se avete già un dispositivo per scoprirne il chipset è sufficiente usare i comandi `lspci` o `lsusb` a seconda di quale interfaccia è usata dalla scheda o chiavetta WiFi. Se non compare nulla di relativo, controllate anche il log del kernel con `dmesg` greppando qualcosa come `wifi`. In lsusb e lspci i nomi sono di solito abbastanza parlanti, e indicano oltre alla marca anche il chip usato, come in questo caso:
```sh
$ lsusb
...
Bus 002 Device 002: ID 7392:7811 Edimax Technology Co., Ltd EW-7811Un 802.11n Wireless Adapter [Realtek RTL8188CUS]
...
```
o in questo:
```sh
$ lspci
...
09:00.0 Network controller: Ralink corp. RT3290 Wireless 802.11n 1T/1R PCIe
...
```

## I buoni e i cattivi
Non tutte le aziende supportano i driver Linux per i propri chipset allo stesso modo. Alcune aziende rilasciano driver liberi che poi sono accettati e inglobati nel kernel, disponibili in modo semplice e in qualsiasi distro. Questo è il caso di Intel e Qualcomm (Atheros), solitamente i migliori come supporto. Altre magari dicono di supportare Linux, ma poi forniscono i sorgenti del modulo del kernel da compilare a mano. Quando è così diffidate: non è per niente detto che il driver riceverà alcun genere di supporto futuro, anzi spesso punta una versione del kernel più vecchia, per cui non è nemmeno detto che compili correttamente. Questo è quel che accade con la maggior parte dei driver Realtek, sebbene esistano driver per alcuni integrati con vari livelli di supporto ufficiale e non.

Potete verificare da voi la bontà della maggior parte dei chipset su questa ottima [pagina Wikipedia](https://en.wikipedia.org/wiki/Comparison_of_open-source_wireless_drivers) oppure cercando l'ID che compare nell'output dei precedenti comandi, ad esempio "7392:7811".

## Sfruttare Amazon
Amazon, e in particolare il suo motore di ricerca e le sue recensioni, è un'ottima risorsa per capire se una scheda almeno funziona per il sistema operativo del nostro benvoluto pinguino. Il modo più semplice è quello di selezionare la giusta categoria, ad esempio *Schede di rete*, e cercare qualcosa come *wifi linux*. Il bravo motore di ricerca ci aiuterà, mostrando i risultati più rilevanti pescando non solo dalla descrizione ma anche dalle recensioni dei clienti. Se siete fortunati qualche anima pia può capitare che abbia pubblicato quale chipset usa, in modo da approfondire quali funzionalità sono supportate dal kernel e se necessita di firmware non libero, se vi interessa.  

Non fatevi spaventare se vedete marche completamente sconosciute ai vostri occhi, tanto l'importante è il chipset che montano le loro schede e che abbiano buone recensioni. Non fatevi nemmeno abbindolare dalle recensioni esageratamente positive, perché spesso queste aziende rimborsano una parte della spesa come buono Amazon se si lascia una recensione positiva. Insomma, leggete le recensioni e non guardate soltanto il punteggio.

## Le mie esperienze
In conclusione vorrei dire la mia sulle schede che ho avuto la possibilità di provare.

Premetto che la mia connessione WiFi è intorno a ~20 Mbit, i vostri risultati potrebbero variare al crescere della velocità.

La prima è quella che avete visto nel primo `lsusb`, ovvero [questa Edimax](https://amzn.to/36yvqpt).

È stato un acquisto obbligato dal momento che la scheda PCI/E integrata ha deciso di lasciarmi (quella vista in `lspci`). Ricordo di averla usata per un po' di tempo su Linux Mint e funzionava normalmente, poi l'ho lasciato per passare credo a Debian e poi ad Arch e non è mai più funzionata, nemmeno in Windows, per cui non dovrebbe essere un problema di driver corrotti o firmware mancanti.

Comunque sto divagando, da quando ho comprato questa chiavetta WiFi non ho mai avuto problemi, è completamente plug&play e riesco a raggiungere il massimo permesso dalla mia banda. Non supporta gli ultimi standard e non è particolarmente economica, per cui vi consiglio di cercare se esiste qualcosa di più moderno.

La seconda è quella integrata nel mio Thinkpad T61, che è Intel quindi ben supportata e che per essere così vecchia (parliamo del 2007) è mini PCI/E quindi potenzialmente più veloce. Il modello è l'Intel PRO 3945ABG, piuttosto vecchia e come dice il nome supporta gli standard a/b/g. Siamo ai limiti della compatibilità, ma funziona e potenzialmente si può sostituire dato che è standard e non saldata, BIOS permettendo. Mi sembra di ottenere velocità un pelo più alte della Edimax, anche grazie al fatto che il Thinkpad ha delle antenne che corrono tutto attorno alla cornice dello schermo. Comunque sia, buona scheda e buona compatibilità con il driver iwl3945.

La terza è l'ultima arrivata ed è una scheda senza pretese per un fisso senza pretese. Parlo di una scheda PCI/E non particolarmente nuova, non particolarmente virtuosa ma economica. Parlo di questa [scheda LTERIVER AR9287](https://amzn.to/35qDpWc), che ha chipset Qualcomm. Ho voluto dargli fiducia perché il venditore ha addirittura scritto in descrizione quali driver Linux usa la scheda (ath9k) e grazie ad alcune recensioni straniere. Insomma, scivolata nello slot e via, funziona perfettamente. 

## Conclusioni
Spero di avervi chiarito le idee su cosa guardare quando acquistate una scheda WiFi per Linux. Se volete essere aggiornati non indugiate e iscrivetevi al feed, per un flusso tutt'altro che costante di articoli circasempre freschi. 
