+++
title = "Come navigare in internet da terminale"
date = 2020-10-22
updated = 2021-02-17
[taxonomies]
tags = [ "browser", "terminale"]
categorie = [ "Software" ]
+++

Immagino che stiate leggendo questo articolo da un browser come Firefox o uno dei tanti basati su Chrome, come Brave.

In altre parole state navigando in rete con un **browser grafico**. In passato invece **si usava il terminale** per recuperare informazioni e navigare, perché fondamentalmente **quasi tutto era in formato testuale**.
Anche se ormai non si può più accedere a tutto da terminale, esiste ancora la possibilità di usare un browser a riga di comando per consultare informazioni testuali e per aprire pagine web.
Anche quando si è connessi ad un server remoto o confinati in una macchina senza GUI un browser testuale può rivelarsi utile.

In questo articolo quindi sono elencati **alcuni browser per il terminale da provare** su Linux.

## I migliori browser testuali per Linux
***Nota:*** *l'ordine di questa lista non ha significato*
### 1. W3M
{{img(src="/images/w3m-google.webp", desc="Google in w3m", aut="Ankush Das , It's FOSS", aut_url="https://itsfoss.com/author/ankush/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}
**w3m** è un famoso browser testuale per il terminale. Anche se il progetto originario non è più sviluppato, una versione è mantenuta viva da uno sviluppatore diverso, Tatsuya Kinoshita.

w3m è abbastanza semplice, **supporta connessioni SSL, colori e anche immagini**. Ovviamente potreste avere dei risultati diversi dai miei, ma da una mia prova veloce sembra non essere in grado di caricare DuckDuckGo, mentre Google funziona perfettamente. (N.d.T.: sulla mia macchina anche DuckDuckGo funziona senza problemi)

Per una pagina d'aiuto basta scrivere `w3m` nel terminale. Se siete curiosi potete anche dare un'occhiata al codice su [GitHub](https://github.com/tats/w3m).

#### Come installare e usare w3m?
W3M è disponibile per la maggior parte delle distro Debian-derivate. Per distro Arch-derivate potete controllare l'AUR se non è disponibile direttamente (N.d.T.: Arch puro ha il pacchetto nel repository extra, default).

Per Ubuntu, si può installare digitando:
```sh
$ sudo apt install w3m w3m-img
```
Questo installa il pacchetto w3m insieme al pacchetto che supporta la resa delle immagini. Dopodiché si può semplicemente eseguire:

```sh
$ w3m xyz.com
```
Ovviamente rimpiazzate xyz.com con un sito qualsiasi di quelli che volete visitare/provare. Infine è importante conoscere come usare i tasti di navigazione. Si usano le frecce per spostarsi e Invio per iniziare un'azione.

Per uscire si può premere `Shift+q` e per andare indietro `Shift+b`. Ulteriori scorciatoie sono `Shift+t` per aprire una nuova scheda e `Shift+u` per aprire un nuovo URL.

Per scoprirne di più si può anche leggere la pagina di man.

### 2. Lynx
{{img(src="/images/lynx-terminal.webp", desc="Google in Lynx", aut="Ankush Das , It's FOSS", aut_url="https://itsfoss.com/author/ankush/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}
**Lynx** è un altro browser testuale che si può provare. I siti che funzionano con Lynx tendono ad essere di più, per cui è indubbiamente migliore su questo aspetto. Sono riuscito a caricare DuckDuckGo e a farlo funzionare. (N.d.T.: vero, ma anche w3m funzionava).

Inoltre ho notato **si possono accettare o rifiutare i cookie** quando si accedono varie risorse in rete. Si può anche **decidere di accettarli o negarli sempre**, per cui questo è un aspetto positivo.

D'altra parte però il contenuto non è riscalato bene alla dimensione della finestra. Non ho cercato soluzioni al problema, potreste volerlo fare voi. Ad ogni modo funziona alla grande e tutti i comandi sono mostrati in bella vista appena lanciato nel terminale.

Notate che il tema del programma non è abbinato al tema del terminale, quindi i colori saranno diversi da quello del vostro tema.

#### Come installare Lynx
Diversamente da w3m esistono anche dei binari Win32 se foste interessati. Su Linux è invece disponibile nella maggior parte dei repository default.

Per Ubuntu, basta digitare:
```sh
$ sudo apt install lynx
```

Per cominciare basta eseguire il comando seguente:

```sh
$ lynx sitoesempio.it
```

Qui basta sostituire il sito d'esempio con la risorsa che si vuole visitare.

Se si vogliono informazioni sui pacchetti nelle altre distro, potete trovare delle informazioni sul [sito ufficiale](https://lynx.invisible-island.net/lynx-resources.html).

### 3. Links2
{{img(src="/images/links2-terminal.webp", desc="Google in Links2", aut="Ankush Das , It's FOSS", aut_url="https://itsfoss.com/author/ankush/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}
**Links2** è un browser testuale interessante per il fatto che si può usare facilmente grazie alla sua buona esperienza utente. È dotato di un'interfaccia curata per inserire l'URL, che poi provvede a lanciare.


{{img(src="/images/links2-terminal-welcome.webp", desc="Modulo per l'URL", aut="Ankush Das , It's FOSS", aut_url="https://itsfoss.com/author/ankush/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}

Da notare che il tema in questo caso è abbinato a quello del terminale (qui è impostato "nero-verde"). Una volta lanciato si può premere il tasto `g` per ottenere il modulo per inserire l'URL, o `q` per uscire. Funziona abbastanza bene e renderizza il testo della maggior parte dei siti.

A differenza di Lynx non si ha la possibilità di accettare/rifiutare i cookie, ma a parte quello funziona bene.

#### Come installare Links2
Come ci si potrebbe aspettare, lo troverete nella maggior parte dei repository default. Per Ubuntu si può installare digitando il seguente comando:

```sh
$ sudo apt install links2
```

Per le altre distro ci si può rivolgere al [sito](http://links.twibright.com/download.php) e alla documentazione ufficiale.

### 4. eLinks
{{img(src="/images/elinks-terminal.webp", desc="Google in eLinks", aut="Ankush Das , It's FOSS", aut_url="https://itsfoss.com/author/ankush/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}

**eLinks** è simile a Links2, ma non è più mantenuto. Si trova ancora in vari repository default, per questo motivo è in lista.

Non adotta il tema del terminale di sistema. Per cui potrebbe non essere una buona esperienza non avendo un tema scuro.

#### Come installare eLinks
Su Ubuntu si installa facile. Si digita:

```sh
$ sudo apt install elinks
```
Per le altre distro dovreste riuscire a trovarlo nei repository ufficiali. Altrimenti potete riferirvi alle [istruzioni di installazione ufficiali](http://elinks.or.cz/documentation/installation.html) se non lo trovate.

{{itsfoss(da="https://itsfoss.com/terminal-web-browsers/", titolo="4 Best Terminal Browsers for Linux Command Line")}}
