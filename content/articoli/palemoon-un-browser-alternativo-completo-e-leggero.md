+++
title = "Pale Moon: un browser alternativo completo e leggero"
date = 2020-05-02

[taxonomies]
tags = ["browser", "internet"]
categorie = ["Software"]
+++

**Pale Moon** è un browser web leggero, ma può dimostrarsi un’alternativa completa ai mainstream Chrome e Firefox. In questo articolo sono riassunte le sue caratteristiche notevoli.

## Funzionalità

Pale Moon è supportato per i sistemi Windows e Linux in modo ufficiale; esiste anche un porting indipendente per MacOS, mentre la versione ufficiale è un cantiere fermo per la mancanza di uno sviluppatore.[1]

Questo browser ha scelto di **tagliare le funzionalità ragionevolmente meno usate** (come l’accessibilità e il controllo genitori)[2]. L’approccio è quello di fornire quando possibile queste funzionalità attraverso estensioni, **allo scopo di favorire una maggiore velocità** all’utente medio.

## Personalizzazione totale

Il motto del programma dice già molto: “*Your browser, your way*”. Questo risultato è raggiunto grazie al continuato **supporto per i plugin XUL**, che sono stati abbandonati da Firefox da ormai tre anni. Questo consente di creare temi originali (non solo skin) e di continuare ad usare quelle estensioni che sarebbero impossibili da implementare con le più recenti **API WebExtension**. Quest'ultime invece **non sono supportate**.

Inoltre è uno dei pochi tra i browser aggiornati che **supporta tutti i plugin NPAPI** (e non solo Adobe Flash)[3], senza avere in programma di abbandonarne il supporto[4].

## E allora per chi è indicato?

Pale Moon non si fa amare a prima vista, con una [pagina iniziale terribile](https://palemoon.start.me/), per cui non è adatto a chi vuole la pappa pronta, ma per **chi non disprezza un minimo di configurazione** può diventare il vostro browser preferito. 

Idealmente è **adatto a computer con risorse ridotte**, quindi PC con architettura x86 vecchiotti (~10 anni) e poca memoria principale (il minimo è 1GB). Uno dei limiti maggiori è la **mancanza dei binari per architetture ARM** (e.g. Raspberry Pi). È sicuramente possibile compilare a mano i sorgenti, ma un browser web non è il programma più banale. Tuttavia questo punto è dipendente dalla dimensione della squadra di sviluppo, che al momento è ristretta.

## Una minima configurazione

Pale Moon **dispone di un’ottima localizzazione**, derivante probabilmente dal suo importante antenato, per cui si può partire dagli **add-on per la lingua**. Il [pacchetto lingua](https://addons.palemoon.org/language-packs/) e il [dizionario](http://repository.binaryoutcast.com/dicts/) per la correzione automatica sono entrambi disponibili in **italiano**.

Per **cambiare la lingua dell’interfaccia** si può andare nella pagina di `about:config` (impostando `general.useragent.locale` ad esempio a `it`) o usando [questa estensione](https://addons.palemoon.org/addon/locale-switcher/) usa e getta. In ogni caso **è necessario riavviare** il programma.

È ora di far sparire quella pessima pagina iniziale. L’ideale sarebbe una soluzione nativa, ma in alternativa un file HTML locale o il motore di ricerca predefinito sono buone soluzioni. Sempre per favorire l’usabilità io nascondo *barra dei menu* (accessibile comunque con `alt`) e *barra dei segnalibri* (disponendo comunque del tasto *Visualizza segnalibri*), ma è completamente facoltativo.

Le estensioni fondamentali per me sono le seguenti:

* [uBlock Origin](https://github.com/gorhill/uBlock-for-firefox-legacy/releases) (da GitHub, ma aggiornato in automatico)
* [Reader View](https://addons.palemoon.org/addon/readerview/), porting della modalità lettura di Firefox
* [Pentadactyl](https://addons.palemoon.org/addon/pentadactyl-community/), per la navigazione Vim-like
* [Greedy Cache](https://addons.palemoon.org/addon/pentadactyl-community/), per forzare sempre l’uso dei documenti in cache

Il tema che uso è [Dark Moon](https://addons.palemoon.org/addon/pentadactyl-community/), un tema minimalista molto elegante.

Alla fine dovrebbe assomigliare a questo:

{{ img(src="/images/palemoon-screenshoot.png", desc="Schermata di Pale Moon appena avviato") }}

Super minimalistico e funzionale.

[1]: https://github.com/MoonchildProductions/Pale-Moon/issues/1715
[2]: http://www.palemoon.org/technical.shtml#features
[3]: https://support.mozilla.org/it/kb/plugin-npapi
[4]: https://www.palemoon.org/roadmap.shtml
