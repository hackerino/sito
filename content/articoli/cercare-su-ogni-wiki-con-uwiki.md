+++
title = "Come cercare su ogni wiki con uwiki"
date = 2020-08-13
draft = true
[taxonomies]
tags = [ "internet", "wiki"]
categorie = [ "Bit utili" ]
+++

Oggi vi descrivo un piccolo progetto che ho costruito per cercare alcune parole chiave in una wiki a scelta, aprendo il risultato nel browser. È parzialmente ispirato dal progetto [suckless](suckless.org), soprattutto per la configurazione, e l'unica dipendenza richiesta è libcurl.

## Configurazione e compilazione
uwiki è scritto in C, quindi non è richiesto nulla di fantasioso, solo gcc e make.

Questo è il momento di personalizzare il file di configurazione, nel file `config.h` in perfetto stile suckless. Ovviamente per cambiare le (poche) impostazioni bisognerà ricompilare.

Le variabili configurabili sono:
* browser: stringa con nome dell'eseguibile di un browser presente nel $PATH
* wiki\_url: URL della wiki da usare di default
* wikis: un vettore di strutture che contengono la coppia di stringhe codice e URL

L'URL da inserire, in entrambi i casi, punta alla pagina delle API di Mediawiki e non alla radice del sito. Questo perché non esiste un URL standard alle API e in ogni sito può essere differente. Nel file preesistente si trovano alcuni esempi. 

L'installazione è quella che vi aspettate, ovvero i classici:
* make
* sudo make install

I sorgenti si possono scaricare [qui](https://gitlab.com/hackerino/uwiki).

## Uso
uwiki è talmente semplice da quasi non aver bisogno di una guida all'uso.

Il formato è il seguente:
```
uwiki [opzioni] parole chiave
```

Si ha a disposizione un'opzione facoltativa, `-w`, che prende uno dei codici definiti nel vettore `wikis` in `config.h` per selezionare il corrispondente URL da usare. Se non si specifica questa opzione si userà semplicemente l'URL `wiki_url` definito in `config.h`.

Per vedere quali sono le wiki disponibili in un determinato eseguibile si usa `-l`, che stampa semplicemente tutte le coppie codice-URL di `wikis`.

Se la ricerca non è ambigua, ovvero se si ha un solo risultato, questo viene aperto nel browser predefinito senza bisogno di interazione; se invece sono disponibili risultati multipli viene richiesto il numero della pagina da aprire. Si può anche specificare con `-b` un browser alternativo. 

## Consigli
uwiki è pensato per avere delle preimpostazioni ragionevoli, in modo da mantenere il comando il più corto possibile. Se però non si ha una wiki principale in cui cercare, può convenire crearsi degli alias al comando con il corretto codice passato con `-w`. Questo si fa nel file rc della shell in uso.

Ad esempio:
```
alias wen='uwiki -w en'
alias warch='uwiki -w arch'
```
