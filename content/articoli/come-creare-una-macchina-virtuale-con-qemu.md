+++
title = "Come creare una macchina virtuale con QEMU"
date = 2020-06-27
[taxonomies]
tags = [ "virtualizzazione", "qemu"]
categorie = [ "Software" ]
+++

**Creare una macchina virtuale** usando **QEMU** può sembrare complicato, ma una volta superato lo scoglio della riga di comando il programma si dimostra non solo semplice ma anche molto potente. Ecco quindi una guida rapida all’uso.

## Creare il file immagine

Il comando di base si presenta così, con l’opzione `-f` per impostare il formato del file. I formati principali sono **raw** e **qcow2**: il primo si comporta come fosse un disco fisico, occupando tutta la memoria, ma è più veloce. Il secondo al contrario ha il vantaggio di non occupare subito l’intero bloccone.

```sh
$ qemu-img create -f qcow2 immagine.img 4G
```

A questo punto si può installare il sistema operativo, facendo vedere a QEMU il file immagine come un CD. È probabilmente necessario aumentare la RAM da assegnare alla macchina virtuale, dato che è preimpostata a 128 MB. L’opzione `-hda` serve a far vedere al sistema il nostro file di disco come hard disk (`hd[a-d]` a disposizione).

```sh
qemu-system-x86_64 -m 1G -hda immagine.img -cdrom live.iso
```

Al primo avvio il disco appena creato non contiene nulla di buono, quindi QEMU avvia la macchina virtuale da cd-rom, mentre dopo l’installazione di un SO l’hard disk prende la priorità massima. Questo permette di installare un sistema operativo sul disco appena creato senza preoccuparsi dell’ordine di boot.

In alternativa si può anche specificare l’opzione `-boot once=d` per effettuare il primo boot da cd-rom e ripristinare l’ordine predefinito dopo un riavvio.
### Immagini derivate

Per testare le applicazioni può essere utile partire da un sistema stabile e crearne uno usa-e-getta, un po’ come accade con le distribuzioni live non persistenti. Sfruttando la modalità *copy-on-write* si può ottenere questo beneficio a un costo minimo di memoria occupata. La memoria occupata dalla nuova immagine infatti non è la stessa dell’originale, ma vengono solo registrate le differenze.

```sh
qemu-img create -f qcow2 -o backing_file=immagine_stabile.img test01.img 
qemu-system-x86_64 -m 256 -hda test01.img
```

N.B. L’immagine stabile deve esserlo nel senso letterale della parola, e non può essere avviata direttamente, anzi è consigliabile renderla di sola lettura, perché si corromperebbero tutte le immagini derivate;

## Migliorare le prestazioni
### KVM

Se il vostro processore supporta Intel VT-x o AMD-V, uno dei due è attivato nel BIOS e il vostro kernel lo supporta allora potete attivare **KVM**, tramite l’opzione `-enable-kvm`.

```sh
qemu-system-x86_64 -m 1G -hda immagine.img -enable-kvm
```

#### Compatibilità e correzione errori

Controlliamo se il processore lo supporta:

```sh
LC_ALL=C lscpu | grep Virtualization
```

Se il processore è supportato ricordatevi di controllare che Intel VT-x/ AMD-V sia attivato nel BIOS per evitarvi inutili mal di testa e imprecazioni.

Controlliamo se il kernel ha a disposizione i moduli richiesti (se l’output è non è vuoto sono disponibili):

```sh
find /lib/modules/$(uname -r) -type d | grep kvm
```

Controlliamo se i moduli del kernel sono stati caricati (se viene mostrato kvm e kvm_intel/kvm_amd bene):

```sh
lsmod | grep kvm
```
Altrimenti li carichiamo i moduli (se presenti):

```sh
sudo modprobe kvm_[intel/amd]
sudo modprobe kvm
```
