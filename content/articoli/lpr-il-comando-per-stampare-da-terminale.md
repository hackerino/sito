+++
title = "lpr: il comando e le opzioni più utili per stampare da terminale"
date = 2020-03-05
[taxonomies]
tags = [ "cups", "terminale"]
categorie = [ "Software" ]
+++

Se usate una stampante in Linux è probabile che usiate **CUPS**, il server di stampa mantenuto da Apple. **lpr** è un’interfaccia a questo server, molto in stile UNIX e supporta tra l’altro la stampa da pipe.

In questo articolo sono riassunte le funzionalità del comando ed è raccolto un elenco delle opzioni più utili.

## Premessa

Questo articolo assume che il vostro **server di stampa** sia **attivo e funzionante**, con tutti i driver necessari. Se così non fosse vi invito ad attendere l’articolo in uscita prossimamente su come configurare il proprio server CUPS.

## Il comando

Ecco qui una raccolta, in ordine di importanza, delle funzionalità supportate da questo comando con la breve spiegazione relativa.

### Selezione stampante

lpr deve sapere a **quale stampante** riferirsi se non specificato e questa si può impostare in diversi modi, consultati in questo ordine:

* variabili d’ambiente LPDEST e PRINTER
* tramite lpoptions
* tramite lpdefault
* graficamente tramite CUPS all’indirizzo <localhost:631>

Una volta impostata la **stampante predefinita** non è necessario inserirla manualmente nel comando. In alternativa si può passare con l’opzione `-P` il nome mnemonico o l’URI della stampante in questione.

### Formati e modi d’input

lpr può ricevere il contenuto da stampare in **diversi formati**, da **file** o da **input standard**: file di testo, PostScript, PDF e immagini.

Quindi è possibile stampare ad esempio un PDF almeno due modi. Così:

```sh
$ lpr file.pdf
```

Oppure così:

```sh
$ cat file.pdf | lpr
```

A questo punto è evidente che si può stampare direttamente l’output di un comando qualsiasi, ad esempio neofetch:

```sh
$ neofetch | lpr
```
### Copie multiple

Molto facile da ricordare. È semplicemente:

```sh
$ lpr -# n file
```

### Opzioni (standard e non)

lpr interpreta un discreto numero di opzioni standard, passate come coppia *chiave=valore* (o anche solo chiave se senza valore da passare) con `-o`. L’elenco completo delle opzioni standard è disponibile nella [pagina man](https://linux.die.net/man/1/lpr-cups).

Esistono anche opzioni specifiche per stampante, il cui elenco si trova con il comando **lpoptions**.

Questo è un riassunto delle opzioni standard più utili:

* page-set=odd – stampa solo dispari
* page-set=even – stampa solo pari
* fit-to-page – riscala alla dimesione della pagina
* number-up=n – stampa n pagine per foglio, con n a scelta tra 1, 2, 4, 6, 9 e 16
* landscape – orientamento orizzontale
* portrait – orientamento verticale
* mirror – stampa il contenuto specchiato, utile per fogli trasferibili
* prettyprint – aggiunge un’intestazione e formatta i file C e C++
