+++
title = "Come accedere alla modalità FEL dei SoC Allwinner"
date = 2020-08-04
draft = true
[taxonomies]
tags = [ "soc", "hardware"]
categorie = [ "Hardware" ]
+++

Scrivo questa guida non tanto perché penso che sia effettivamente utile a qualcuno, ma più come riferimento per me stesso del futuro.

La FEL è una modalità di trasferimento dati che ricorda per certi versi la modalità download dei telefoni Android. Per la maggior parte delle schede SBC questa modalità è disponibile tramite la porta USB OTG (micro USB).

Questa modalità è parte della BROM, che è contenuta in un chip fisico. Per questo motivo è disponibile in ogni caso, e questo la rende perfetta per la prima programmazione di una scheda non supportata.

Esiste una collezione di tool per interagire con le schede basate su AllWinner, sunxi-tools. Tra i vari tool a noi ne serve uno in particolare, sunxi-fel. Per verificare di essere in modalità FEL basta controllare se risulta un dispositivo Allwinner con `lsusb`. Per verificare di poter effettivamente comunicare si può usare `sunxi-fel version`, che chiede la versione del protocollo in uso al chip. 
