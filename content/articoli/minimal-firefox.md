+++
title = "L'estensione Minimal e alcuni pensieri su trucchi psicologici e internet obeso"
date = 2021-03-24
[taxonomies]
tags = [ "internet", "tecnologia obesa"]
categorie = [ "Software" ]
+++

L'altro giorno mi sono imbattuto in questo video di [Brodie Robertson](https://odysee.com/@BrodieRobertson:5/minimal-saner-web-browsing-for-a-better:8), che vi consiglio di seguire per scoprire nuovo software per Linux, sull'[estensione Minimal](https://minimal.aupya.org). A quanto pare non ha bisogno della mia grandiosa pubblicità, dato che ha ricevuto una [buona copertura dai giornali francesi](https://minimal.aupya.org/#more_press), immagino che sia l'unico software che sviluppano oltralpe (non prendetevela, in Italia siam messi peggio).

Dal punto di vista software non è tutta quest'innovazione: di fatto **elimina elementi di disturbo dalla pagina** sfruttando classi e id; possiamo pensarla come un'evoluzione del blocca-pubblicità. Inoltre si propone di **prevenire gli sprechi di tempo**, che sono massimizzati in particolare sui social network tramite trucchi psicologici per motivi commerciali: più tempo si spende sul sito maggiore è il numero di pubblicità viste e conseguentemente maggiore è anche il profitto. 

A volte è proprio necessario usare ad esempio YouTube, ma si rischia di venire risucchiati nella trappola e finir per perdere delle ore. L'esempio migliore di ciò è la **riproduzione automatica**, che sfrutta la **piacevole passività** e l'**effetto Zeigarnik** per tenerti incollato, tramite un meccanismo simile al completismo nei videogiochi: l'ho iniziato, voglio finirlo tutto (vedi [video su Arte.tv](https://www.arte.tv/it/videos/085801-005-A/youtube-guardare-senza-pensare/)). A questo scopo l'estensione è davvero utile perché impedisce questo meccanismo e porta sempre a compiere scelte consce, perciò vi consiglio di scaricarla.

L'aspetto più interessante però non è tanto tecnologico, ma più filosofico: il fatto che **quest'estensione in pratica fornisce un'interfaccia alternativa** a [vari siti](https://minimal.aupya.org/#about_changes) mi ha portato a pensare: perché l'interfaccia deve una e così strettamente legata al contenuto? Perché non può essere personalizzata a livello di browser? Chiaramente per alcuni progetti più artistici sarebbe impensabile, ma per la maggioranza non c'è la necessità di personalizzare il sito a livelli estremi. 

Una soluzione sarebbe quella per i browser di **implementare un foglio di stile di base gradevole**: alla fine la maggior parte dei siti riusano gli stessi stili, ad esempio quelli di Bootstrap. Questo permetterebbe ad esempio di **scegliere tra tema chiaro e scuro su ogni sito**. Questo dubito che avverrà mai per motivi di retrocompatibilità, ma potrebbe essere una modalità alternativa attivabile con un metatag o qualcosa di simile.

Un'alternativa potrebbe essere quella di usare lo strumento giusto per il lavoro: il browser oggi è un enorme coltellino svizzero, che fa di tutto, ma malamente. Questo sembra essere il problema risolto dai **protocolli del cosiddetto piccolo internet**, come il nonno [Gopher](https://en.wikipedia.org/wiki/Gopher_(protocol)) e il giovane [Gemini](https://en.wikipedia.org/wiki/Gemini_(protocol)). Entrambi sono protocolli specializzati su un lavoro e lo fanno bene: consegnano **UNA** pagina ipertestuale, punto.

Gemini propone anche un'alternativa leggera e limitata ad HTML, `text/gemini`, che è **simile a Markdown** e quindi si può scrivere direttamente a mano, come si faceva con l'HTML, che qualche nostalgico si ricorderà insieme alle GIF animate e ai tag marquee. Il formato permette anche di inserire collegamenti a immagini, audio etc., ma da specifica **non possono essere caricati automaticamente senza interazione dell'utente**. Usare questo genere di protocolli servirebbe anche a ridemocratizzare internet, perché rende fattibile se non addirittura facile **scriversi il proprio server e soprattutto il proprio client**, al contrario di obesità come Chrome e Firefox che sono assolutamente impensabili anche solo avvicinare partendo da zero. Questo approccio si potrebbe replicare e usare per ogni genere di contenuto: un protocollo per canali video su internet, uno testo e notizie (che potrebbe essere anche Gemini), uno per i negozi elettronici: quanto sarebbe figo poter accedere a tutti i prodotti di tutto il web da un'unica app aggregatrice, tipo RSS? 

Abbiamo bisogno di alternative alle piattaforme odierne che legano l'utente e lo costringono a vita ad un servizio. **Ricostruiamo un internet libero per tutti!** 
