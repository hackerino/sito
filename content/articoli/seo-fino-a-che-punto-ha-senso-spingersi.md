+++
title = "SEO: fino a che punto ha senso spingersi?"
date = 2020-03-14

[taxonomies]
tags = ["seo", "blog"]
categorie = ["Riflessioni"]
+++

Quando si vuole **generare traffico** per la propria attività, che sia un blog o un sito aziendale, il consiglio unanime è quello di **puntare sulla SEO**, ovvero l’ottimizzazione verso i motori di ricerca. In questo articolo quindi voglio mostrare la mia opinione, senza pretendere di essere nel giusto, sui contenuti e la loro preparazione.

## La (mia) qualità

Non sono di certo un poeta, ma passo parecchio tempo a rimuginare sulle parole giuste da inserire in un determinato contesto. Questo perché **voglio costruire contenuti scorrevoli e allo stesso tempo utili**.

Personalmente **credo che i contenuti più fruibili siano quelli più diretti**, che arrivano al punto senza perdere il tempo di entrambi, lettore e scrittore. Penso che i muri di testo esistano essenzialmente per tre motivi:

1. il testo non è diviso in modo efficiente;
2. chi scrive non ha le idee chiare;
3. c’è interesse ad allungare il brodo;

Il primo punto è sicuramente il più facile da risolvere: **si divide il testo in modo logico** (con sottotitoli, paragrafi ecc.) in modo da aiutare il lettore a non perdersi e di permettergli di potersi fermare e ripartire.

Anche la soluzione al secondo non è difficile, ma richiede un po’ più di tempo **per studiare in profondità l’argomento** di cui si parla. È un problema grave, perché il lettore se si accorge che chi scrive gira intorno all’argomento senza comunicare nulla cerca altrove.

Arrivando al terzo punto, se chi scrive ha tutto l’interesse a creare pacchetti d’aria fritta allora la soluzione è delegata al lettore. Ci sono professionisti che da anni **studiano ogni strategia per riuscire a parlare di molto poco**, diluendo quel poco in un mare di parole e riuscendo anche a non infastidire chi legge. In questo caso **conviene sempre chiedersi** a intervalli regolari: “*Mi sta parlando di qualcosa di concreto oppure sono solo frasi fatte e battute?*”. Se la risposta non è la prima allora si sta solo sprecando tempo.

## Il compromesso

Non è facile trovare il giusto **compromesso tra lunghezza e leggibilità**, perché minori parole implicano più termini tecnici e viceversa. Sono arrivato alla conclusione che lo **stile Wikipedia** sia quello vincente: ogni volta che si incontra un termine tecnico che potenzialmente chi legge può non conoscere inserisco un link (se possibile interno) a un articolo di spiegazione. In questo modo **si può scendere in profondità e risalire a piacimento** nella maglia di contenuti.

Esiste anche la possibilità di creare un **dizionario o jargon** che, in stile indice analitico dei libri tecnici, **raccoglie le pagine** che **spiegano quel determinato argomento**. In ogni sito minimamente specializzato secondo me può essere un utile punto di riferimento, anche se alla SEO non importa nulla e per lei è solo una pagina come un’altra. La difficoltà è nel fatto che non credo di aver mai visto una pagina simile, quindi **sarebbe da implementare da zero**.

Forse sul breve termine **risparmiare caratteri sembra una scelta stupida**, considerando che gli utenti spendono di conseguenza meno tempo sul sito, ma sul lungo periodo confido che siano in grado di scegliere il **contenuto migliore**.

## Rispetto per chi legge

Usando WordPress esistono n-mila *plugin* per il SEO e tutti insistono con **l’inserire dappertutto le parole chiave**. Per quanto possa essere una scelta sensata sul breve termine, sono invece convinto che anche questa mossa sia a dir poco **deleteria sul lungo periodo**.

Chi compie questa scelta sfrutta una particolare versione di un algoritmo che in un particolare momento storico ha il monopolio. Sebbene in questo periodo è difficile immaginare Google detronato dal suo ruolo in futuro tutto è possibile. **Le tecnologie cambiano** e le **piattaforme online**, che non sono altro che una particolare implementazione, **nascono e muoiono** di conseguenza.

**Chi scrive un contenuto su internet lo scrive per sempre**, perciò deve riuscire a immaginarlo vivo anche a distanza di anni. Per questo motivo non ha senso farcire i contenuti di parole chiave solo per favorire l’algoritmo, **rendendo ridicolo il testo** agli occhi del pubblico. Bisogna ricordarsi che le impressioni negative sono molto più forti di quelle positive, quindi il proprio nome sarà difficile da rendere nuovamente autorevole.

## In conclusione

Sono convinto che sia la **qualità dei contenuti a fare la differenza** in un mondo in cui tutti possono dire la propria. Per questo motivo eviterei di costruire articoli attorno alla SEO di un determinato momento storico attorno soltanto per guadagnare traffico. Questo comportamento è tipico dei siti di fake news e sono convinto che in futuro i contenuti clickbait finiranno perlopiù per essere ignorati (io personalmente passo oltre).

**Alcuni concetti** però, come la suddivisione logica del testo, **si possono apprendere e integrare** all’interno di ogni articolo, **senza in alcun modo snaturare il contenuto**. In questi casi, se chi scrive ritiene che apportino valore al contenuto, ha senso intraprendere queste buone abitudini.
