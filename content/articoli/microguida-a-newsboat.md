+++
title = "Microguida a Newsboat"
date = 2020-07-08
[taxonomies]
tags = [ "feed", "rss"]
categorie = [ "Software" ]
+++

**Newsboat** è un aggregatore dall’interfaccia basata su **ncurses**. Per usarlo al meglio è necessaria una minima configurazione iniziale. Ha **due file** di configurazione, entrambi nella cartella `$HOME/.config/newsboat/`.

Il **primo** è `config`, dove si possono **ridefinire i tasti** per ogn azione e **configurare il browser** da utilizzare per aprire gli url. Il mio file è il seguente:

```
# $HOME/.config/newsboat/config

browser openinbrowser
auto-reload yes
```

* *auto-reload* è usato per riscaricare tutti i feed all’apertura.
* *browser* setta il programma usato per aprire i link, deve essere presente nel `$PATH`

Nel mio caso invece ho creato script molto rozzo per usare diversi programmi in base all’URL ed è il seguente:

```sh
#!/bin/sh

[ ! -z "$(echo $1 | grep youtube)" ] && mpv --fs --ytdl-raw-options=format=best[height=720] $1 && exit
[ ! -z "$(echo $1 | grep .mp3)" ] && mpv --fs $1 && exit
[ ! -z "$(echo $1 | grep ansa.it)" ] && ansa $1 | less && exit
[ ! -z "$(echo $1 | grep xkcd.com)" ] && xkcd $1 && exit
firefox $1
```

`$1` contiene l’url ricevuto da Newsboat, con varie grep filtro se l’url contiene una certa chiave, se sì lo apre con il programma corretto ed esce. Se dopo aver testato ogni pattern il link è sconosciuto lo apre con firefox. Si può interpretare come un semplice switch-case con un ramo default che esegue **firefox**. I comandi `ansa` e `xkcd` non li troverete in alcun repository, ma sono solo semplici script basati su Xidel, del quale ho già parlato ampiamente in passato.

Il **secondo** e ultimo file da configurare è `urls`, sempre nella stessa cartella, ed è il file più banale del mondo: uno per riga vanno scritti i link ai feed a cui siete interessati. Ad esempio (stavolta non metto il mio, ficcanaso):

```sh
# $HOME/.config/newsboat/config

# Blog
http://feeds.feedburner.com/MarcoTogniBlog "blog"
# Notizie
http://www.ansa.it/sito/ansait_rss.xml "nuove"
```

Da notare solo due cose:

1. le righe iniziate con `#` sono considerate commenti;
2. la stringa tra virgolette, facoltativa, consente di **filtrare per etichetta** premendo `t` nel programma.

**Fine**. A questo punto potete dimenticarvi della configurazione (come ho fatto io fino ad oggi, quando sono andato a rispolverarla dall’hard disk). Come al solito in caso di dubbi leggete il [man](https://www.mankier.com/1/newsboat).
