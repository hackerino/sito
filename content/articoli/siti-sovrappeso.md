+++
title = "Ci servono siti più leggeri"
date = 2021-02-25
draft = true
[taxonomies]
tags = [ "web", "internet"]
categorie = [ "Web" ]
+++

* incipit // video luke smith
* raggiungere dispositivi più vecchi
* disparità connessione internet (reti veloci accentrate)
* impatto ambientale
* conclusione

La loro non sarà di sicuro la stessa prospettiva degli utenti del lavoro finito, costretti su collegamenti appena meglio di [IPoAC](https://en.wikipedia.org/wiki/IP_over_Avian_Carriers). 

I siti internet sono diventati o stanno diventando sempre più pesanti. Anche senza guardare le statistiche la sensazione è forte e la tendenza si nota. Quando poi (eresia!) capita di usare un browser senza il blocco delle pubblicità (metà dei browser per smartphone, e mi vien da piangere), è l'inferno. Ignorando per una volta tutti i problemi di raccolta sistematica di dati personali, concentriamoci invece sul come siti internet abbiano messo su qualche rotolo di JavaScript di troppo e sul perché invece sarebbe sensato andare verso siti più veloci, ottimizzati e accessibili a tutti. 

## Raggiungere dispositivi più vecchi
Il software, come la tecnologia in generale, aumenta di complessità anche dove non è necessario. In più, ma questa è una mia impressione, molti sviluppatori web non si mettono nei panni dei propri utenti, che non sono seduti in un comodo ufficio con computer aziendali sostituiti regolarmente. Anche il crescente uso di JavaScript non necessario rallenta i siti web per l'utente inesperto o disinteressato, che non sa come bloccare le pubblicità e i traccianti finendo per cambiare smartphone, buttando quello vecchio in un cassetto. Inoltre a volte queste "funzionalità" sono puramente estetiche, di moda, senza un reale vantaggio per l'utente. Quest'ultima si può anche vedere come la gentrificazione di internet. Esiste anche chi non ha la possibilità economica, ma che è costretto a rincorrere la tendenza per non rimanere completamente fuori dal mondo e tagliato fuori dalle possibilità che internet fornisce. Questo è triste e completamente evitabile, ma questo è il risultato quando si pensa soltanto alla maggioranza.    

## Disparità di connessione
Questa è una mia fissazione. Così come la disparità sociale sarà il tema del secolo, anche la disparità di connessione è un'ipotesi che non saprei nemmeno come verificare, ma che almeno logicamente regge. Chiamatela pure la congettura di Dotto: la velocità media aumenta, ma la velocità mediana cresce molto più lentamente. La logica è banale: nelle grandi città ci sono connessioni stratosferiche che tirano su la media, ma i poveri disgraziati che per dovere o per scelta vivono fuori da un grande centro urbano subiscono miglioramenti modesti o nulli. Un altro esempio è il 5G, che sarà anche agli inizi ma la copertura è e rimarrà indecente: raggiungeremo presto un limite per cui sarà completamente irragionevole tappezzare di antenne ogni angolo delle città e delle campagne. Per questo quando sviluppate, ricordatevi della peggiore connessione che avete trovato, non soltanto della connessione media; esiste anche una funzione di Firefox che permette di limitare la velocità per testare come si comporta in lentezza: usatela.

## Impatto ambientale
Questo è un tema tanto importante quanto nascosto, nascosto dietro infiniti strati di astrazione. Nessuno di noi ha la minima idea di quale sia il consumo energetico di tutta la catena che parte dal server e che ci permette di fruire del contenuto, così come nessuno ha la minima idea di quanto 


## Gli opposti
Un esempio dice poco, ma come punto di partenza prendiamo il dato del peso del sito. Stiamo parlando poi di un sito di notizie, non di una piattaforma interattiva dove per forza di cose è necessario usare un certo tipo di tecnologie per rendere il sito dinamico e utile. Ecco quindi come in questa prospettiva 8MB per una pagina sembrano ridicoli. 

{{ img(src="/images/open-peso.png", desc="Esempio di dati trasferiti dal sito open.online con ad-blocker disattivato")}}

## Pubblicità e tracker
È evidente che le pubblicità e i traker si prendono una grossa fetta della torta in termini di dati consumati, senza fornire nemmeno un bit utile all'utente. Ovvio, le pubblicità sono "necessarie" per un certo modello di guadagno, e tracker e analitiche varie permettono di conoscere il proprio pubblico, però contribuiscono entrambi a gonfiare la conta dei dati trasmessi. Non basta, perché anche tutto il JavaScript aggiuntivo che deve essere eseguito rallenta visibilmente il caricamento dei contenuti. 

Per rendersi conto di quanto questo aspetto influisca basta installare un qualsiasi adblocker, uBlock Origin è la mia scelta, e andare in un sito impestato di pubblicità, ad esempio Wired. Il guadagno in velocità è notevole, soprattutto da cellulare. Purtroppo è un gioco alla gatto e topo, e non sono pochi i siti che hanno sistemi per bloccare gli adblocker. In ogni caso generalmente i risultati si vedono.

## Video automatici
Parliamoci chiaro, non penso che esista una persona al mondo che dopo aver visto uno di quei video che partono in automatico poi lo ha guardato davvero. E perché gli sviluppatori pensano che sia una buona idea? Per pompare i numeri? Questo mi ricorda quei siti oscuri che si trovano in internet che pagando una certa cifra si può generare traffico al proprio sito, solitamente per sfruttare i sistemi di pubblicità. Come se servisse a qualcosa. Quello che serve è il cosiddetto traffico organico, quello che si guadagna con la dimostrazione di qualità, questo sistema al massimo può far perdere i lettori guadagnati che se ne vanno esasperati. Non ne so molto di SEO, ma questo è buon senso.

Inoltre, se si usa una connessione con dati limitati, beccarsi un video, che magari non si nota neanche in un angolo tipo Wired –oggi ce l'ho con loro– incide sul traffico mensile e anche sul numero di bestemmie giornaliere quando me ne accorgo troppo tardi. 

## Marchettari, marchettari ovunque
Si capisce che volete prendere la mia mail per farla finire in qualche giro di newsletter che mi intasa la casella di posta perché sono troppo pigro per disiscrivermi, ma si può anche fare in modo educato in un angolo o in fondo all'articolo o nel piè di pagina, e non per forza con un'animazione di una casella di testo da riempire in faccia. In genere questo problema lo si risolve sempre con un adblocker, ma resta il fatto che il sito era pessimo in partenza e l'adblocker lo rende a malapena usabile.

Un altro esempio sono le richieste di iscriversi tramite notifiche e no, non mi iscriverò mai a nessuna vostra notifica per poi dovermi ritrovare la cronologia delle notifiche passate sul PC o ancora peggio il menu a tendina del cellulare imbottito di notifiche in mezzo alle quali magari ne ho una di veramente utile e di interesse. 

Trovo anche penoso questo invogliamento alla distrazione, dove si guadagna in modo proporzionale al tempo perso dalla persona, e si sfrutta ogni strategia per farlo abboccare.

## Siti di notizie che si credono web-app
Questa è la categoria peggiore, ovvero quei siti che sono essenzialmente solo testo e immagini, ma se disattivi JavaScript ti becchi una pagina bianca o quando sei fortunato un messaggio di errore che ti prende per il culo dicendoti che non siamo nel medioevo. Capisco benissimo l'esigenza di essere diversi, ma distinguersi per rendere un sito inutilizzabile non è un buon modo di distinguersi, soprattutto quando si ha a che fare con testo e immagini. Non serve caricare e posizionare dinamicamente tutti gli elementi di una pagina di notizie: le web-app sono ben utili, penso ad Office nel browser, ma non per un sito di notizie. 

A volte anche quando si ha un sito ragionevolmente ben costruito si incontrano problemi: ad esempio si nota spesso che si hanno contenuti che compaiono scorrendo, come nel feed di vari reti sociali. Questo è un meccanismo che primo non degrada sempre in modo piacevole perché genera dei siti obrobriosi dove ad esempio non si riesce a raggiungere il fondo. Secondo, tornando al punto precedente, questo è un meccanismo studiato e sfruttato per creare dipendenza ed è piuttosto triste trovarlo. Posso comprenderlo in un motore di ricerca, dove scorrendo compaiono nuovi risultati, ma non in un giornale online. 

## Conclusione
In questo articolo ho elencato alcune delle cose che non mi piacciono di internet allo stato attuale. Sono certo che peggiorerà, ma comunque già oggi ci sono vari esempi positivi e da seguire che potrebbero mostrare il modo giusto a chi costruisce un nuovo sito. Penso a Wikipedia e a tutti i suoi progetti fratelli, a siti come Il Post (fanno tutto giusto, non sono come mai) e Ansa. Spero in un internet più leggero o alla separazione tra internet leggero e pesante, in modo che possano convivere ma con compiti separati. 
