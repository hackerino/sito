+++
title = "Come parsificare l’HTML di ogni sito con Xidel"
date = 2020-01-17
[taxonomies]
tags = [ "internet", "parsificare"]
categorie = [ "Software" ]
+++

Ho sempre desiderato uno strumento da integrare in semplici script per **parsificare i file HTML**, ma non ho mai approfondito la questione. Dopo navigando l’internet sono naufragato in uno strumento non molto famoso chiamato **Xidel**. Parte della sua non-fama è dovuta dal fatto di essere **scritto in Pascal**, praticamente un linguaggio morto.

## HTML ma non solo

Ogni terminalista che si rispetti ha usato nella sua vita un browser testuale come **Lynx**. Nonostante il risultato sia leggibile in alcune situazioni (da lodare Wikipedia e DuckDuckGo), in varie altre (non per sua mancanza) risulta fastidioso cercare l’informazione desiderata in mezzo a infiniti menu, intestazioni, piè di pagina ecc. Qui entrano in gioco linguaggi come **XPath**, semplice ma efficace per individuare elementi specifici di file XML. Una breve guida la si trova su [Wikipedia](https://it.wikipedia.org/wiki/XPath). Xidel si è dimostrato inoltre più robusto dei concorrenti **xmlstarlet** e **xmllint** per quanto riguarda i file HTML.

Un caso d’uso è il *feed RSS* di [Ansa](https://www.ansa.it/sito/static/ansa_rss.html), che usato in combinazione con Newsboat è molto comodo, ma da terminale le sue pagine sono quasi illeggibili. Con Xidel posso **estrarre il paragrafo** contenente il testo della notizia in modo superfacile, con un solo comando e senza strane *regex*.

```sh
$ xidel -s "$url" -e "//div[contains(@class, 'news-txt')]"
```

XPath (v3) non è l’unico linguaggio di query supportato, infatti sono disponibili anche XPath2, XQuery3, XQuery1, JSONiq e CSS, oltre a un linguaggio originale del programma. Per quanto riguarda i file oltre a HTML è possibile parsificare XML e JSON.

## Limiti e difetti

Questo strumento è molto comodo, ma non è perfetto. Per esempio in interrogazioni XPath dove si richiede più di un *tag* **non si dimostra flessibile** per niente sull’ordine dell’*output*, che viene mantenuto lo stesso della pagina originale. Dato che Pascal è arabo per me non sono andato a metterci il naso, ma è una scelta comprensibile. Così operando infatti non si ha bisogno di spazio in memoria per il risultato da elaborare in seguito, ma si stampa appena trovato il filtro richiesto. Ciò nonostante risulta limitante in alcune situazioni.

Altro difetto, soprattutto per principianti, è il fatto di essere **poco presente nei repository**. Ad oggi il pacchetto è presente solo in Arch (sull’AUR, tra l’altro obsoleto), quindi bisogna scaricare i file precompilati e copiarli in una cartella di `$PATH` sul sito oppure compilarlo manualmente.

## Compilazione manuale

Data la natura singolare del programma e delle dipendenze (essendo scritto in Pascal) non si tratta di un processo banale.

Si può riassumere nei seguenti passi:

* `git clone https://github.com/benibela/xidel.git && cd xidel`
* aprite con un editor il file .travis.yml e eseguire manualmente i comandi dell’etichetta `install`:, adattandoli eventualmente al proprio gestore di pacchetti (nel file viene usato apt)
* `./build.sh -t`
* se emergono altri problemi di dipendenze assenti installatele e tornate al passo precedente
* `sudo ./install.sh`
