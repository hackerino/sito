+++
title = "Come ho migliorato questo sito con Zola"
description = "In questo articolo parlo della mia transizione a Zola, un generatore di siti statici, e di come ho risolto o risolverò alcuni problemi che ho incontrato."
date = 2020-07-09
[taxonomies]
tags = [ "internet", "web"]
categorie = [ "Blog" ]
+++

Ho appena finito la conversione di questo sito da WordPress a un generatore di siti statici, [**Zola**](https://www.getzola.org/).

In passato avevo già provato [**Jekyll**](https://jekyllrb.com/), ma dopo un po' ho lasciato perdere essenzialmente perché non volevo avere niente a che fare con le dipendenze di Ruby, che non avevo mai visto e sarebbe stata un'ulteriore cosa da imparare oltre al linguaggio di template e tutto il resto, e non ne avevo per nulla voglia.

Zola mi ha fatto subito innamorare di sé per vari motivi:

* è distribuito come singolo eseguibile senza dipendenze
* è scritto in un linguaggio compilato (Rust)
* usa la versione più standard di Markdown ([Commonmark](https://commonmark.org/))
* supporta in modo nativo la generazione di feed, sitemap
* gestisce in semi-automatico paginazione e tassonomie (categorie, tag e/o altre personalizzate)
* linguaggio di template ragionevolmente semplice e leggibile ([Tera](https://github.com/Keats/tera))

La migrazione non è stata ovviamente automatica e indolore, ma sono comparsi una serie di problemi, primo fra tutti **il tema**. Non avendone trovato uno che soddisfasse tutti i miei requisiti (non molti per la verità: buon CSS responsivo, graficamente minimalista, paginazione e categorie, no JS) ho deciso di modificare quello che a mio avviso è ad oggi il migliore: [hyde](https://github.com/getzola/hyde), un tema portato da Jekyll tra l'altro. Dopo aver capito la logica che sta dietro a Zola è stato in realtà molto semplice apportare le modifiche necessarie, prendendo anche spunto da altri temi come [slim](https://github.com/jameshclrk/zola-slim) (che era più vicino ai miei requisiti ma graficamente noioso). Vale la pena notare che senza l'**ottima documentazione** di cui dispone non sarebbe stato altrettanto semplice. 

Risolta la questione tema rimane ancora la **traduzione in Markdown** degli articoli, che ho deciso di fare a mano, approfittando dell'occasione per imparare una volta per tutte la formattazione con questo linguaggio, e ho trovato immensa gioia nel poter usare **vim** come editor di testo al posto di quel marciume di editor a blocchi presente in WordPress. **Commonmark** ha davvero tutte le funzionalità che mi servono, tranne forse due: le didascalie sotto le immagini e un sistema di note a piè di pagina. In WordPress era nativa la prima e la seconda me la cavavo con un'estensione. È anche vero che Zola ha una soluzione elegante a questo problema: **le abbreviazioni** (*shortcode*), che per le didascalie sono una soluzione banale (me ne rendo conto ora) e con un pizzico di JS anche le note si possono implementare in modo ragionevolmente facile (quel che è importante per me è che la parte principale del blog funzioni anche senza JS, trovo assurdo quando non viene mostrato nemmeno il testo della pagina).

L'ultima grande sfida è quello che WordPress poteva fare in modo ridicolmente semplice avendo a disposizione un DB: la **gestione dei commenti**. Una soluzione sarebbe quella di integrare qualcosa come **Disqus** nella pagina ad esempio con un iframe, ma preferirei una soluzione autogestita. Se avete suggerimenti sentitevi liberi di commentare qui sot... Ops, non l'ho fatto apposta, lo giuro... Comunque per suggerimenti il mio contatto per il momento è l'email: [questa](mailto:simone@nigge.rs).

Grazie per l'attenzione, spero di avervi annoiato a sufficienza con i miei problemi, ma di avervi almeno fatto scoprire un pezzo di software che è entrato sicuramente nella lista dei miei programmi preferiti. Prossima tappa: guida pratica alla creazione di un sito con Zola.
