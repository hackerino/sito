+++
title = "Le basi di Vim"
date = 2020-08-06
#draft = true
[taxonomies]
tags = [ "vim", "editor"]
categorie = [ "Software" ]
[extra]
toc = true
+++

**Vim** è un editor di testo basato sullo storico vi, presente in praticamente ogni sistema UNIX, che sia Linux o BSD. Questa è un'introduzione di base alla logica e ai comandi più utili dell'editor.

## Prima di iniziare
Avvertimento. Imparare ad usare un editor come Vim non è molto diverso dall'imparare la dattilografia: per impararlo bisogna rallentare. 
Può essere frustrante se si deve portare a termine un lavoro subito, ma una volta imparato ripaga largamente il tempo investito. Serve prendersi tempo per praticare, perché è tutto quel che serve per costruire la memoria muscolare necessaria. Al contrario della dattilografia, di Vim si può imparare soltanto quel che serve: bisogna sapere dove sono tutte le lettere sulla tastiera perché si useranno tutte, ma sicuramente non tutti i comandi di Vim. È per questo motivo che esiste questa guida, che raccoglie il sottoinsieme di comandi che (personalmente) ritengo più utili.

Menzione onorevole per vimtutor, la guida integrata di Vim, che è un buon punto di partenza ma che non mette abbastanza l'enfasi sulla logica di Vim e sui comandi solitamente usati.

## Le modalità: normale, inserisci e visuale
Vim dispone di tre modalità profondamente diverse nello scopo. La **modalità normale** (anche chiamata modalità di comando) è quella in cui ci si trova quando si avvia l'editor. In questa modalità ogni tasto è associato a un ben preciso comando, con una ben precisa funzione: muovere il cursore nel testo, copiare una linea di testo, incollarne un'altra, cancellare una porzione di testo. Arrivando da un'altra modalità si può tornare a quella normale con il tasto `[ESC]`.

Dalla modalità normale si può accedere alla **modalità inserisci**, ad esempio con il tasto `i`. In questa modalità si –incredibilmente– inserisce del testo, nel punto indicato dalla barra verticale.

Dalla modalità normale si può accedere alla **modalità visuale**, ad esempio con il tasto `v`. Questa modalità si usa principalmente per selezionare porzioni particolari di testo, usando gli stessi comandi della modalità normale per il movimento.

### Modalità normale
#### Muoversi
Per spostarsi nel testo si possono usare le frecce, ma nessuno lo fa. Quel che si usa invece sono i tasti `h/j/k/l` che significano in ordine `sinistra/giù/su/destra`. Sono tasti molto comodi –almeno su QWERTY– che però non hanno alcun vantaggio rispetto a un editor normale. Entrano in gioco i tasti `w`, `e` e `b`, che si usano nell'ordine per muoversi all'inizio della parola successiva, alla fine della stessa parola e all'inizio della parola precedente.

È bene imparare fin da subito che spesso **i comandi possono essere preceduti da numeri**, che servono per eseguire lo stesso comando più volte. Ad esempio `3j` scorrerà per tre righe in basso, mentre `4w` mi porrà all'inizio della parola che dista quattro parole dalla posizione attuale.

Ci sono anche comandi per altri movimenti particolari, come quelli per inizio e fine riga. Per muoversi all'inizio della riga si usa `0`, mentre per la fine si usa `$`. Non sono molto importanti di per sè, ma saranno utili combinati con altri comandi, per esempio quello per cancellare.

Esistono comandi per spostarsi tra le righe del documento. Per farlo si usa `numero_rigaG`, ad esempio per andare alla riga 10 del file sarà `10G`. Per muoversi all'inizio del documento si usa `gg`, mentre per il fondo si usa `G`.

#### Copiare e incollare
Il copia e incolla in Vim ruota intorno ai comandi `y` e `p`, con `y` che sta per *yank* e `p` per *paste*. Quando si copia una porzione di testo con `y`, il testo selezionato è memorizzato temporaneamente in un buffer, e può in seguito essere incollato in giro con `p`.

Quando si preme `y` in realtà non accade nulla, infatti Vim si mette in attesa di ulteriori specifiche su cosa copiare. Il comando che si sta inserendo è mostrato in basso a destra. Per selezionare si usa `y` abbinato a un comando di movimento: ad esempio `yw` copia una parola, `y3w` ne copia tre, `y2b` copia le due parole precedenti e così di seguito. 

Alcune combinazioni utili sono:
* yy — copia una riga
* yap — copia (attorno) un paragrafo
* y0 — copia fino a inizio riga
* y$ — copia fino a fine riga
* ygg — copia fino all'inizio del documento
* yG — copia fino a fine documento

Una volta riempito il buffer di selezione, per incollare le opzioni sono essenzialmente due: `p` e `P`. `p` si usa praticamente sempre, e incolla dopo la riga attuale, mentre `P` incolla dopo la riga attuale; si usa praticamente solo per incollare alla prima riga del file. 

#### Cancellare/tagliare
In Vim il concetto di cancellare e tagliare non sono completamente distinti; quando si cancella del testo è automaticamente salvato anche nel buffer di copia, pronto per essere incollato da qualche altra parte, anche se non verrà mai usato.

Il comando per cancellare è `d` e funziona esattamente allo stesso modo di `y`; se si vuole incollare si usano sempre `p` e `P`.

Da notare che `D` è l'equivalente di `d$`, mentre `dd` cancella l'intera riga.

#### Annulla/rifai
Per annullare l'ultima modifica in modalità inserisci, si preme `u`. Se si vuole invece rifare quanto annullato si può premere `CTRL+R`.

#### Cerca e sostituisci
Per cercare in Vim si usa `/`, e si completa il termine di ricerca, che può essere indifferentemente semplice oppure un'espressione regolare. Per cercare all'indietro si usa `?` allo stesso modo. Per scorrere i risultati si usano `n` per il successivo e `N` per il precedente. 

Per sostituire una parola o espressione regolare si usa un comando composto,`:%s/termine/sostituzione/g`. Il `%` indica l'intero file, quindi si potrebbe sostituire con, ad esempio, un numero di riga `n` o un range di righe `a-b`. La `g` indica che la sostituzione è globale, per chiedere una conferma per ogni sostituzione si può aggiungere in coda `c` che sta appunto per *confirm*. 

### Modalità inserisci
#### Semplice inserimento
Per inserire semplicemente del nuovo testo partendo prima del carattere sotto il cursore, si preme `i`. Per capire come funziona, si può provare a premere `i` e `[ESC]` di seguito: il cursore viaggerà all'indietro. Per inserire invece del testo dopo al carattere attuale si preme `a`, che sta per *append*.

Da notare anche i comandi `I`, per iniziare l'inserimento da inizio riga, e `A`, al contrario da fine riga.

#### Nuova riga
Per creare una nuova riga e entrare in modalità di inserimento si usa `o`, che crea una riga successiva a quella attuale. In alternativa si può usare `O` per una riga precedente a quella attuale.

#### Modifica
Il comando modifica combina i comandi di cancellazione e inserimento. Il comando base è `c`, che sta per *change*, ed usa la stessa identica sintassi di `y` e `d`. Tra i comandi più utili in questo caso c'è `caw` per modificare una parola.

### Modalità visuale
La modalità visuale in generale serve per evidenziare del testo, per poi aggiungerlo al buffer di copia premendo semplicemente `y` o `d`.

#### Visuale semplice
La modalità visuale semplice è una modalità a cui si accede con `v`. Ci si muove dal punto in cui è stata invocata la modalità per evidenziare il testo nelle vicinanze con i comandi di movimento di Vim. 

#### Visuale per righe
Ci si accede con `V`. Funziona allo stesso modo di `v`, ma si possono soltanto evidenziare intere righe, indipendentemente dalla posizione del cursore sulla riga.

#### Visuale blocco
Ci si accede con `CTRL+v`. È la duale della visuale per righe nel senso che si evidenziano intere colonne, il che può essere utile lavorando su tabelle, ma anche ad esempio per modificare in parallelo porzioni di righe con `c`.

## Uscire e salvare
In Vim per uscire e salvare si usano comandi con il duepunti. Quelli che si usano più spesso sono `:w` per salvare senza uscire, `:x` per salvare e uscire e `:q!` per uscire senza salvare.
