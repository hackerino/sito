+++
title = "Readability + Newsboat = il meglio"
date = 2021-01-23
[taxonomies]
tags = [ "feed", "rss", "terminale"]
categorie = [ "Software" ]
+++

In passato ho già parlato di come configurarsi in pochi passi quello che secondo me è il miglior aggregatore di notizie, Newsboat. Lo trovate [qui](microguida-a-newsboat.md). Oggi vi presento un aggiornamento di quella configurazione, che permette di fruire le notizie dal terminale con una comodità inaudita grazie a readability-cli, un porting del codice di Firefox per la **modalità lettura**, appunto per la linea di comando.

## Installazione
Darò per scontato che abbiate già Newsboat installato, nel caso fate riferimento all'altro articolo che vi dicevo poco sopra.

Readability-cli è scritto in JS, dato che il suo codice è basato sulla versione scritta per Firefox, quindi è disponibile come **pacchetto npm**:
```sh
$ npm install -g readability-cli
```
In alternativa, per gli **utenti Arch o Arch-basati**, esiste anche il pacchetto AUR, installabile ad esempio con `yay`:
```sh
$ yay -S readability-cli
```

## Uso
L'eseguibile del pacchetto `readability-cli` si chiama in realtà `readable`. Si usa essenzialmente richiamato come `readable $x`, dove $x può essere il percorso a un file HTML o un URL, e sputa fuori sullo standard output l'HTML ripulito di tutti gli elementi inutili per la lettura, come i menu, moduli di contatti, popup ecc. Questo HTML si può incanalare in un browser testuale come **w3m** per avere il testo formattato nel terminale. w3m ha bisogno dell'opzione '-T 'text/html' perché da solo non riconosce il formato e lo intende come testo semlice.

Esistono poi anche **alcune opzioni**, come `-o` per specificare un file di output o tutta una serie di parametri, passati con `--properties`, per stampare solo una parte delle pagina come il titolo o il contenuto. Il valore predefinito di questi parametri è 'html-title,html-content' e difficilmente avrete bisogno di cambiarlo. 

## Configurazione
La mia configurazione (~/.config/newsboat/config) è semplice, oserei dire banale, come potete vedere:
```
browser openinbrowser
auto-reload yes
feed-sort-order title
bind-key j next
bind-key k prev
bind-key J next-feed
bind-key K prev-feed
```
In pratica l'unica variabile di interesse è browser, che indica quale browser è usato quando si preme `o` in newsboat. Nel mio caso è impostata a openinbrowser, che è uno script che discrimina in base all'URL quale programma deve essere lanciato. Si presenta così:
```
#!/bin/sh
[ ! -z "$(echo $1 | grep youtube)" ] && mpv --fs --ytdl-raw-options=format=best[height=720] $1 && exit
[ ! -z "$(echo $1 | grep .mp3)" ] && mpv --fs $1 && exit
[ ! -z "$(echo $1 | grep xkcd.com)" ] && xkcd $1 && exit
readable '$1' | w3m -T 'text/html'
```

Se andate a confrontare questo con quello della scorsa volta, l'unica cosa che cambia è il browser di default (ovvero l'ultimo, quello più generico quando tutti gli altri test falliscono) era **Firefox**. Ora ho semplicemente sostituito Firefox con `readable $1 | w3m -T 'text/html'`. Volendo potete anche farvi uno scriptino, ma le opzioni sono poche da non renderlo necessario. 

## Risultato
Per concludere, ecco un esempio di come si vede la stessa pagina dell'*ANSA* nel terminale prima di passarla in readable e dopo. Alla prossima!

{{ cimg(src="/images/orwell-prima.png", desc="Articolo prima, non si vede nemmeno il contenuto") }}
{{ cimg(src="/images/orwell-dopo.png", desc="Articolo dopo, perfetto") }}
