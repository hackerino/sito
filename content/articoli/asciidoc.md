+++
title = "AsciiDoc: guida completa in Linux"
date = 2020-10-15
[taxonomies]
tags = [ "formattazione", "asciidoc", "tutorial"]
categorie = [ "Guide" ]
+++

Questa guida dettagliata discute i vantaggi derivanti dall'uso di AsciiDoc, mostrando come installarlo e usarlo in Linux.

## Perché usare AsciiDoc (o altri formati testuali)?
Vedo due vantaggi nello scrivere usando formati testuali: per primo, c'è una separazione chiara tra il contenuto e la sua presentazione. Questo argomento è aperto a discussioni, considerato che alcuni formati testuali come TeX o HTML richiedono una certa disciplina per mantenere detta separazione. Contemporaneamente si può già ottenere un certo grado di separazione con [modelli e fogli di stile](https://wiki.openoffice.org/wiki/Documentation/OOo3_User_Guides/Getting_Started/Templates_and_Styles) usando editor WYSIWYG. Lo riconosco. Nonostante ciò trovo lo stesso fastidiosi i problemi di presentazione con strumenti GUI. Laddove, usando formati testuali, ci si potrebbe concentrare sul solo contenuto, senza che lo stile del font o la barra della finestra ti disturbino nella scrittura. Ma forse sono solo io? Tuttavia non posso contare il numero di volte che ho smesso di scrivere solo per risolvere qualche problema di stile secondario, perdendo l'ispirazione dopo essere tornato al testo.

Comunque sia, il secondo argomento è meno soggetto all'interpretazione personale: i documenti basati su formati testuali sono altamente interoperabili. Non solo si possono modificare con qualsiasi editor su qualsiasi piattaforma, ma si possono gestire facilmente le revisioni con strumenti come [git](https://en.wikipedia.org/wiki/Git) o [SVN](https://en.wikipedia.org/wiki/Apache_Subversion), o automatizzare la loro modifica con strumenti tipici come [sed](https://en.wikipedia.org/wiki/Sed), [AWK](https://en.wikipedia.org/wiki/AWK), [Perl](https://en.wikipedia.org/wiki/Perl) e così via. Per fare un esempio concreto, usando un formato testuale come AsciiDoc serve un solo comando per produrre lettere ben personalizzate partendo da un documento maestro, mentre lo stesso lavoro usando un editor WYSIWYG avrebbe richiesto un uso consapevole di "campi" e oltrepassare varie schermate di procedura guidata.  

## Cos'è AsciiDoc
Per essere rigorosi AsciiDoc è un formato di file. Esso definisce costrutti sintattici che aiuteranno l'elaboratore a comprendere la semantica delle varie parti del testo, di solito al fine di produrre un output ben formattato.

Anche se questa definizione può sembrare astratta stiamo parlando di un qualcosa di semplice: alcune parole chiave o caratteri hanno un significato speciale, che cambia la resa del documento prodotto. È lo stesso concetto dei tag in HTML, una differenza chiave però è che il documento sorgente rimane facilmente leggibile.

Vedi questo [repository GitHub](https://github.com/itsfoss/asciidoc-intro/tree/master/coffee) per confrontare come lo stesso output può essere prodotto usando alcuni formati testuali comuni. (grazie a [Linux Journal](https://www.linuxjournal.com/article/1158) per l'idea della pagina man del caffè)

* `coffee.man` usa il venerabile *troff* (basato su [RUNOFF](https://en.wikipedia.org/wiki/TYPSET_and_RUNOFF), programma del 1964). Oggi è usato quasi soltanto per scrivere [pagine di man](https://en.wikipedia.org/wiki/Man_page). Per provarlo, avendo prima scaricato i file `coffee.*`, si scrive `man ./coffee.man` alla riga di comando.
* `coffee.tex` usa la sintassi *LaTeX* (1985) per ottenere praticamente lo stesso risultato, ma per un PDF. LaTex è un programma di formattazione orientato particolarmente alle pubblicazioni scientifiche, per la sua abilità di formattare bene formule matematiche e tabelle. Si può produrre il PDF dai sorgenti LaTeX con `pdflatex coffee.tex`
* `coffee.html` usa il formato *HTML* (1991) per descrivere la pagina. Potete aprire direttamente il file con il vostro browser preferito per vedere il risultato.
* `coffee.adoc`, infine, usa la sintassi *AsciiDoc* (2002). Si può produrre sia HTML che PDF da quel file:

```sh
$ asciidoc coffee.adoc			# HTML in uscita
$ a2x --format pdf ./coffee.adoc	# PDF in uscita (dblatex)
$ a2x --fop --format pdf ./coffee.adoc	# PDF in uscita (Apache FOP)
```

Dopo aver visto il risultato, apri i quattro file con il tuo fidato editor di testi (nano, vim, SublimeText, gedit, Atom,...) e confronta i file sorgenti: converrai con me che i sorgenti in AsciiDoc sono più facili da leggere e probabilmente anche da scrivere.

{{img(src="/images/troff-latex-html-asciidoc-compare-source-code.webp", desc="Confronto tra i vari sorgenti che raggiungono (circa) lo stesso risultato", aut="Sylvain Leroux, It's FOSS", aut_url="https://itsfoss.com/author/sylvain/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}

## Come installare AsciiDoc in Linux?
AsciiDoc è relativamente complicato da installare a causa delle molte dipendenze. Con complicato si intende compilandolo, per il resto di noi usare il gestore di pacchetti è probabilmente la maniera migliore:

Per Ubuntu e simili:
```sh
$ apt-get install asciidoc fop
```

Per Fedora:
```sh
$ yum install asciidoc fop
```

Per Arch:
```sh
$ pacman -S asciidoc fop
```

(fop è richiesto solo se vi serve [Apache FOP](https://en.wikipedia.org/wiki/Formatting_Objects_Processor) come backend per la generazione dei PDF)

Più dettagli sull'installazione possono trovarsi sul [sito ufficiale di AsciiDoc](http://www.methods.co.nz/asciidoc/INSTALL.html). Per il momento tutto ciò che serve è un po' di pazienza visto che, almeno sul mio sistema Debian minimale, l'installazione di AsciiDoc ha bisogno di scaricare 360MB (soprattutto a causa delle dipendenze LaTeX). Il che, a seconda della banda che avete, potrebbe darvi tempo in abbondanza per finir di leggere l'articolo. 

## AsciiDoc tutorial: come scrivere in AsciiDoc?
L'ho detto varie volte, AsciiDoc è un formato testuale leggibile. Per cui si possono scrivere documenti usando un editor di testo a scelta. Esistono persino editor dedicati. 

Non ho intenzione di scrivere un altro tutorial sulla sintassi AsciiDoc: ne esistono innumerevoli nel web. Per questo menzionerò solo i costrutti di base, quelli che si usano in praticamente tutti i documenti. Partendo dall'esempio "coffee" qui sopra, si può notare che:
* i **titoli** in AsciiDoc si identificano dai sottostanti `===` o `---` (a seconda del livello)
* il **grassetto** è racchiuso da asterischi
* e il **corsivo** da trattini-bassi

Sono convenzioni abbastanza comuni che probabilmente vengono dall'epoca delle email pre-HTML. Inoltre potrebbero servire altri due costrutti comuni, non mostrati nel precedente esempio: **collegamenti** e **immagini**, la cui sintassi è abbastanza auto-esplicativa:  

```
// Collegamenti ipertestuali 
link:http://dashing-kazoo.flywheelsites.com[ItsFOSS Linux Blog]

// Immagini in linea
image:https://itsfoss.com/wp-content/uploads/2017/06/itsfoss-text-logo.png[ItsFOSS Text Logo]

// Immagini a blocco
image::https://itsfoss.com/wp-content/uploads/2017/06/itsfoss-text-logo.png[ItsFOSS Text Logo]
```

Ma la sintassi di AsciiDoc è molto più ampia. Se volete più di questo, posso indirizzarvi a una bella [pagina di trucchi](http://powerman.name/doc/asciidoc).

## Come produrre l'output definitivo?
Assumerò che si abbia a disposizione del testo scritto in AsciiDoc. Se questo non è vero potete scaricare [qui](https://raw.githubusercontent.com/itsfoss/asciidoc-intro/master) dei file di esempio, tratti dalla documentazione AsciiDoc:

```sh
# Scarico il sorgente della guida all'utente di AsciiDoc
$ BASE='https://raw.githubusercontent.com/itsfoss/asciidoc-intro/master'
$ wget "${BASE}"/{asciidoc.txt,customers.csv}
```
Dato che AsciiDoc è leggibile, si può inviare il sorgente direttamente per email e il destinatario sarà in grado di leggere quel messaggio senza fare null'altro. Ma si potrebbe volere un output formattato meglio. Per esempio un HTML per la pubblicazione web (proprio come ho fatto per questo articolo), o come PDF per la stampa o la lettura su schermo.

In ogni caso serve un *elaboratore*. In realtà, sotto sotto, serviranno vari elaboratori, perché il documento AsciiDoc sarà convertito in vari formati intermedi prima di produrre l'output finale. Dato che sono usati molti strumenti, con l'output di uno passato come input del successivo, si parla a volte di *catena di montaggio* (toolchain).

## Nella pratica?
Per la resa in HTML serve solo `asciidoc`. Per catene di montaggio più complicate consiglio di usare `a2x` (è distribuito con AsciiDoc), il quale penserà ad avviare gli elaboratori necessari in ordine:

```sh
# Tutti gli esempi sono basati sulla guida all'utente di AsciiDoc

# Resa in HTML
asciidoc asciidoc.txt
firefox asciidoc.html

# Resa in XHTML
a2x --format=xhtml asciidoc.txt

# Resa in PDF (elaborato da LaTeX)
a2x --format=pdf asciidoc.txt

# Resa in PDF (elaborato da FOP)
a2x --fop --format=pdf asciidoc.txt
```

Anche se si può produrre direttamente dell'output in HTML, la funzionalità principale di `asciidoc` è quella di trasformare dal formato AsciiDoc al formato intermedio [DocBook](https://en.wikipedia.org/wiki/DocBook). DocBook è un formato basato su XML, usato di solito (ma non solo) per la produzione di documentazione tecnica. DocBook è un formato semantico, ciò significa che descrive il contenuto del documento, ma *non* la sua presentazione. Per questo motivo, qualsiasi sia il formato dell'output, il formato DocBook intermedio è passato ad un elaboratore [XSLT](https://en.wikipedia.org/wiki/XSLT) per generare direttamente l'output (e.g. XHTML) o un altro formato intermedio.

Questo è quel che capita quando si genera un PDF, dove il documento DocBook sarà convertito come rappresentazione intermedia o a LaTeX o a [XSL-FO](https://en.wikipedia.org/wiki/XSL_Formatting_Objects) (un linguaggio basato su XML per descrivere le pagine). Finalmente uno strumento dedicato può convertire quella rappresentazione in PDF.

I passaggi extra per la generazione di PDF sono particolarmente giustificati dal fatto che la catena di montaggio ha da gestire la paginazione dell'output in PDF. Questa operazione non è necessaria per un formato "fluido" come ad esempio HTML.

### dblatex o fop?
Considerando che ci sono due backend per la resa in PDF, la solita domanda è "Qual è il migliore?". Questa domanda non ha risposta.

Entrambi gli elaboratori hanno [pregi e difetti](http://www.methods.co.nz/asciidoc/userguide.html#_pdf_generation) e, la scelta finale sarà un compromesso tra bisogni e gusti, per cui il consiglio è di provarli entrambi prima di scegliere backend da usare. Se segui la via di LaTeX, sarà usato [dblatex](http://dblatex.sourceforge.net/) per produrre il PDF, mentre sarà [Apache FOP](https://xmlgraphics.apache.org/fop/) se preferisci lavorare con il formato intermedio XSL-FO. Perciò non dimenticare di dare un occhio alla documentazione di questi strumenti, per vedere quanto sarà facile adattare l'output alla tua volontà. A meno che (duh!) non siate già soddisfatti dalla resa di default.

## Come personalizzare l'output di AsciiDoc
### AsciiDoc a HTML
Di base AsciiDoc produce documenti abbastanza belli, ma prima o poi vorrai personalizzarne l'aspetto.

Le modifiche precise dipendono dal backend usato. Per l'output HTML, la maggior parte delle modifiche sono fatte modificando il foglio di stile [CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets) associato al documento.

Per esempio, se volessi vedere rosse tutte le intestazioni, potrei creare il seguente file `custom.css`:

```css
h2 {
	color: red;
}
```

E processare il documento usando una versione lievemente modificata del comando:

```sh
# Imposta l'attributo 'stylesheet' al percorso assoluto al nostro file CSS personalizzato
$ asciidoc -a stylesheet=$PWD/custom.css asciidoc.txt
```

Si possono anche fare modifiche ad un livello più fine aggiungendo un attributo *role* all'elemento. Questo sarà tradotto in una classe nell'HTML generato.

Ad esempio, prova a modificare il nostro documento di prova aggiungendo l'attributo 'role' al primo paragrafo del testo:

```
[role="summary"]
AsciiDoc is a text document format ....
```

Poi aggiungi la seguente regola in `custom.css`:

```css
.summary {
    font-style: italic;
}
```

Rigenera il documento:

```sh
$ asciidoc -a stylesheet=$PWD/custom.css asciidoc.txt
```
{{img(src="/images/asciidoc-html-output-custom-role-italic-paragraph-color-heading.webp", desc="Resa in HTML con un file CSS personalizzato", aut="Sylvain Leroux, It's FOSS", aut_url="https://itsfoss.com/author/sylvain/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}

Et voilà: il primo paragrafo è ora mostrato in corsivo. Con un po' di creatività, pazienza e un paio di tutorial sul CSS dovreste essere in grado di personalizzare i documenti a vostro piacimento.

### AsciiDoc a PDF
Personalizzare l'output PDF è un po' più complicato. Non dal punto di vista dell'autore, dato che il testo rimane lo stesso, eventualmente si può usare l'attributo 'role' come sopra per identificare le parti che hanno bisogno di un trattamento speciale.

La differenza è che non si può più usare il CSS per stabilire la formattazione del PDF reso. Per la maggior parte delle impostazioni più comuni, esistono parametri che possono essere impostati da riga di comando. Alcuni parametri possono essere usati con dblatex e fop allo stesso modo, altri sono specifici ad uno dei due.

Per una lista dei parametri supportati da dblatex, vedi <http://dblatex.sourceforge.net/doc/manual/sec-params.html>.

Per una lista dei parametri supportati da DocBook XSL, vedi <http://docbook.sourceforge.net/release/xsl/1.75.2/doc/param.html>.

Dato che la regolazione dei margini è una richiesta piuttosto comune, si potrebbe voler dare uno sguardo per quello: <http://docbook.sourceforge.net/release/xsl/current/doc/fo/general.html>.

Sebbene i nomi dei parametri sono più o meno congruenti tra i due backend, gli argomenti usati per passare tali valori sono diversi tra *dblatex* e *fop*, per cui è bene controllare la sintassi se qualcosa sembra non funzionare.

Meritevole di menzione è che l'uso di font non standard, richiede del lavoro aggiuntivo, anche con *fop*, ma è abbastanza ben documentato sul sito di Apache: <https://xmlgraphics.apache.org/fop/trunk/fonts.html#bulk>.

```
# XSL-FO/FOP
$ a2x -v --format pdf \
    --fop \
    --xsltproc-opts='--stringparam page.margin.inner 10cm' \
    --xsltproc-opts='--stringparam body.font.family Helvetica' \
    --xsltproc-opts='--stringparam body.font.size 8pt' \
    asciidoc.txt

# dblatex
# (body.font.family dovrebbe funzionare, ma evidentemente non va ?!?)
$ a2x -v --format pdf \
    --dblatex-opts='--param page.margin.inner=10cm' \
    --dblatex-opts='--stringparam body.font.family Helvetica' \
    asciidoc.txt
```

### Impostazioni granulari per la generazione di PDF
I parametri globali sono comodi se serve modificare qualche impostazione predefinita, ma se si vuole migliorare finemente il documento (o cambiarne completamente l'organizzazione) serviranno ulteriori sforzi.

Nel motore di elaborazione di DocBook c'è [XSLT](https://en.wikipedia.org/wiki/XSLT). XSLT è un linguaggio per computer, espresso in notazione XML, che permette di descrivere qualsiasi trasformazione di un documento XML in ... qualcos'altro. XML o meno.

Ad esempio, bisognerà modificare o integrare il foglio di stile default di [DocBook XSL](http://www.sagehill.net/docbookxsl/) che produce il codice XSL-FO al fine di creare nuovi stili. Invece se si usa il backend *dblatex* questo può voler dire modificare il corrispettivo foglio di stile XSLT per la conversione DocBook-a-Latex. Nell'ultimo caso si potrebbe anche aver bisogno di un pacchetto LaTeX personalizzato. Qui non mi soffermerò su *dblatex*, ma se si vuol saperne di più questa è la [documentazione ufficiale](http://dblatex.sourceforge.net/doc/manual/sec-custom.html).

Anche se ci concentriamo solo su *fop*, è troppo lungo elencare tutta la procedura, per cui è mostrato solo il necessario per raggiungere un risultato simile a quello ottenuto con le poche righe di CSS nell'output in formato HTML di prima. Cioè: le intestazioni in rosso e il paragrafo di *sommario* in corsivo.

Il trucco che uso qui per creare un nuovo foglio di stile è di importare quello originale di DocBook ma sovrascrivendo i set di attributi o i template per gli elementi che voglio cambiare:

```xml
<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common" exclude-result-prefixes="exsl"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>

<!-- Importo il foglio di stile DocBook predefinito per XSL-FO -->
<xsl:import href="/etc/asciidoc/docbook-xsl/fo.xsl" />

<!--
    DocBook XSL definisce un sacco di set di attributi che si
    possono usare per controllare gli elementi in output
-->
<xsl:attribute-set name="section.title.level1.properties">
    <xsl:attribute name="color">#FF0000</xsl:attribute>
</xsl:attribute-set>

<!--
    Per modifiche granulari, si dovrà o scrivere o
    sovrascrivere i template XSLT come ho fatto sotto
    per i simpara 'summary' simpara (paragrafi)
-->
<xsl:template match="simpara[@role='summary']">
  <!-- Catturo l'ereditato -->
  <xsl:variable name="baseresult">
    <xsl:apply-imports/>
  </xsl:variable>

  <!-- Personalizzo il risultato -->
  <xsl:for-each select="exsl:node-set($baseresult)/node()">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="font-style">italic</xsl:attribute>
      <xsl:copy-of select="node()"/>
    </xsl:copy>
  </xsl:for-each>
</xsl:template>


</xsl:stylesheet>
```
Poi bisogna chiedere a `a2x` di usare quello specifico foglio di stile, al posto di quello predefinito, per produrre l'output, usando l'opzione `--xsl-file`:
```sh
$ a2x -v --format pdf \
    --fop \
    --xsl-file=./custom.xsl \
    asciidoc.txt
```
{{img(src="/images/asciidoc-fop-output-custom-role-italic-paragraph-color-heading.webp", desc="Resa in PDF con XSLT personalizzato", aut="Sylvain Leroux, It's FOSS", aut_url="https://itsfoss.com/author/sylvain/", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/4.0/")}}

Prendendo un po' la mano con XSLT, con i consigli dati qui e qualche domanda al tuo motore di ricerca preferito dovresti essere in grado di iniziare a personalizzare l'output XSL-FO.

Non mentirò, modifiche del documento apparentemente semplici possono richiedere un discreto lasso di tempo scrollando manuali di DocBook e XSL-FO, esaminando i fogli di stile sorgenti e facendo due esperimenti prima di raggiungere finalmente il risultato voluto.

{{itsfoss(da="https://itsfoss.com/asciidoc-guide/", titolo="Complete Guide for Using AsciiDoc in Linux")}}
