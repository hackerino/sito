+++
title = "Come riusare lo smartphone come adattatore WiFi"
date = 2020-12-17
[taxonomies]
tags = [ "android", "tethering", "wifi"]
categorie = [ "Guide" ]
+++

Chiunque ha in casa un vecchio smartphone in disuso, a cui magari si è affezionato e che dispiace un po' buttare. In questa breve guida vedremo un modo di recuperare il proprio smartphone Android come scheda WiFi esterna per il PC, sfruttando il tethering e un'impostazione un po' nascosta del proprio cellulare.

## Passo 1: attivare le opzioni di sviluppatore
Alcuni di voi sapranno già di cosa sto parlando e l'avranno già attivate, comunque sia le opzioni di sviluppatore (o di sviluppo, o simili in base al modello) sono delle impostazioni tenute nascoste da Android all'utente comune. Tra di queste ne troviamo una di nostro interesse, che sarà discussa nel prossimo punto, per questo è necessario abilitarle e renderle accessibili.

Farlo è semplice, basta recarsi nelle impostazioni sotto la voce *Informazioni sul dispositivo* e dopo toccare circa 10 volte sulla riga denominata *Numero build*. Dopo alcuni tocchi il telefono reagirà e mostrerà un messaggio del tipo *Mancano x tocchi per sbloccare le opzioni di sviluppatore*, che è un'ulteriore conferma che la riga di informazione è quella giusta.

Ora nell'elenco delle categorie di impostazioni troveremo anche la voce sbloccata, e siamo pronti per il prossimo punto.

## Passo 2: impostare il tethering automatico
{{ cimg(src="/images/USB_predefinito_1.png", desc="Impostazione Configurazione USB predefinita") }}

Tra il numero immenso di opzioni di sviluppo troviamo la voce di interesse, sotto il nome di ***Configurazione USB predefinita***. Questa impostazione, come si intuisce dal nome, consente di decidere l'azione automatica da intraprendere quando il telefono è connesso via cavo USB. La scelta opportuna nel nostro caso è ovviamente *Tethering USB*, fatto questo si può uscire e abbiamo concluso.

{{ cimg(src="/images/USB_predefinito_2.png", desc="Scelte per la configurazione automatica dell'USB") }}

Adesso collegando il telefono al WiFi potrete sfrutttare il vostro telefono-bidone in un ragionevole adattatore WiFi, ad esempio per usarlo connesso ad un computer fisso, senza dover comprare un dispositivo apposta. Non si risparmia granché, ma perché sprecare? Alla prossima guida rapida, qui è ssmao, ciao.
