+++
title = "Come inserire lettere accentate in Linux con il tasto compose"
date = 2020-02-15
[taxonomies]
tags = [ "tastiera", "xorg"]
categorie = [ "Linux" ]
+++

In questo articolo vedremo come impostare il **tasto compose in Xorg** per inserire praticamente ogni carattere immaginabile senza dover ogni volta fare copia e incolla.

## Cos’è il tasto compose

Il **compose** è un tasto che, quando premuto, attiva una modalità di inserimento particolare usata per inserire un carattere non presente sulla tastiera tramite una sequenza mnemonica di altri caratteri.

Detto così sembra molto complicato, ma vediamo subito un esempio. Per inserire il carattere spagnolo **ñ** mi basta premere in sequenza (non contemporaneamente) i tasti `compose`+`n`+`~`.

Nonostante la logica sia come quella appena mostrata, a volte non si riesce a trovare come si scrive un determinato carattere solo a buon senso. Una **buona lista** che può aiutarvi in questi casi la trovate su [fsymbols](https://fsymbols.com/keyboard/linux/compose/).

Ora mi direte: sulla tastiera non vedo alcun tasto compose. Avete perfettamente ragione, infatti questo era presente in origine solo in workstation con sistema UNIX, invece Windows e MacOS non lo supportano in modo nativo e quindi è caduto nel dimenticatoio.

{{ img(src="/images/compose-key.jpg", desc="Tasto compose su una tastiera Sun Type 5 and 5c", desc_url="https://commons.wikimedia.org/wiki/File:Compose_key_on_Sun_Type_5c_keyboard.jpg", aut="Felix the Cassowary", lic="CC BY-SA", lic_url="https://creativecommons.org/licenses/by-sa/3.0/deed.en") }}

## Come impostarlo

In **Xorg** il tasto compose è preimpostato come la combo Shift+Alt Gr (in questa sequenza, scambiati sono un altro tasto), che lo rende abbastanza scomodo per un uso quotidiano.

Per scoprire quali sono i tasti disponibili da impostare basta eseguire il comando:

```sh
$ grep "compose:" /usr/share/X11/xkb/rules/base.lst
```

Un esempio di output sul mio sistema è il seguente:

```
  compose:ralt         Right Alt
  compose:lwin         Left Win
  compose:lwin-altgr   3rd level of Left Win
  compose:rwin         Right Win
  compose:rwin-altgr   3rd level of Right Win
  compose:menu         Menu
  compose:menu-altgr   3rd level of Menu
  compose:lctrl        Left Ctrl
  compose:lctrl-altgr  3rd level of Left Ctrl
  compose:rctrl        Right Ctrl
  compose:rctrl-altgr  3rd level of Right Ctrl
  compose:caps         Caps Lock
  compose:caps-altgr   3rd level of Caps Lock
  compose:102          &lt;Less/Greater&gt;
  compose:102-altgr    3rd level of &lt;Less/Greater&gt;
  compose:paus         Pause
  compose:prsc         PrtSc
  compose:sclk         Scroll Lock
```

A questo per impostare il tasto prescelto basta eseguire il seguente comando (ad esempio con **Caps Lock**):

```sh
$ setxkbmap -option compose:caps
```
Io stesso uso questa configurazione e trovo che sia molto comoda, anche perchè Caps Lock è un tasto davvero grande in una posizione comoda e non viene usato spesso.

Una buona alternativa può essere Alt Gr, che a discapito di una minore comodità permette di mantenere il blocco delle maiuscole per le rare volte che serve.

Bisogna notare che questo comando ad ogni riavvio viene dimenticato, quindi conviene aggiungerlo al proprio file `.xinitrc`, in modo che sia eseguito all’avvio di Xorg.
