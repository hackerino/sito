+++
title = "La libreria più veloce del C (dipende dai punti di vista)"
date = 2020-12-13
draft = true
[taxonomies]
tags = [ "c", "programmazione"]
categorie = [ "Bit utili" ]
+++

Questa settimana credo di essermi reso conto per la prima volta del perché il C può essere odioso. Sappiamo tutti che in C bisogna i gestire puntatori e calibrare bene la lunghezza dei vettori per non sovrascrivere parti di memoria di altre variabili se non di peggio (che belli i *segmentation fault*). Non è questo il problema però, perché errori di questo genere per programmi piccoli e per uso personale si possono ignorare, oppure semplicemente si impara a non sbagliare più dopo aver sbagliato abbastanza volte. 
