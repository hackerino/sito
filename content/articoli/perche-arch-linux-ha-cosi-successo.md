+++
title = "Perché Arch Linux ha così tanto successo?"
date = 2020-08-27
[taxonomies]
tags = [ "linux", "arch"]
categorie = [ "Linux" ]
+++

BTW, I use Arch!

È facile trovare queste parole in forum, discussioni o meme su Linux.

Potresti chiederti: "Perché Arch Linux è così popolare?". Perché piace così tanto alla gente quando esistono distro basate su Arch che sono, se non migliori, almeno più facili?

In questo articolo sono elencati alcuni dei motivi per cui agli utenti piace usare [Arch Linux](https://www.archlinux.org/).

## 6 motivi per cui Arch Linux piace
Prima di cominciare, questa è la mia impressione. Non c'è un motivo particolare per cui uno debba usare Arch Linux. Questo è ciò che ho osservato nella mia esperienza ultradecennale con gli utenti Linux e le sue community.

Vediamo perchè Arch Linux è così popolare.

### 1. L'approccio fai-da-te dà il controllo su ogni aspetto del sistema operativo
Ho sempre trovato Arch Linux un sistema operativo fai-da-te. Dall'installazione alla gestione, Arch Linux permette di gestire tutto.

Puoi decidere quale *desktop environment* usare, quali componenti e servizi installare. Questo controllo granulare ti dà un sistema operativo minimale su cui costruire con elementi scelti liberamente.

Se sei un amante del fai-da-te, amerai Arch Linux.

### 2. Con Arch Linux si ottiene una comprensione migliore di come funziona Linux
Se hai mai provato ad installare Arch Linux, conoscerai la complessità che si porta dietro.

Questa complessità significa anche però che sarai costretto ad imparare cose che non ti sarebbero importate in altre distro.

Ad esempio, configurare la rete durante l'installazione di Arch Linux è una bella lezione.

Se ti senti sopraffatto, l'[Arch Wiki](https://wiki.archlinux.org/) aspetta. È una documentazione impressionante, la più completa su internet, ed è gestita dalla community. Anche solo una scorsa veloce nell'Arch Wiki ti insegnerà un sacco di roba. 

### 3. Gli ultimi kernel e software con il modello a rolling release
Arch Linux è una distribuzione rolling release. Ciò significa che le nuove versioni di kernel e software sono disponibili non appena vengono rilasciate.

Mentre la maggior parte delle altre distro Linux forniscono una vecchia versione del kernel, Arch è veloce a rendere disponibile l'ultimo kernel.

Lo stesso vale per il software. Se viene rilasciata una nuova versione di un software che è nei repository Arch, gli utenti otterranno la nuova versione prima degli altri la maggior parte delle volte.

Nel modello rolling release tutto è fresco e all'avanguardia. Non serve aggiornare il sistema operativo da una versione all'altra. Basta usare `pacman` per avere sempre l'ultima versione.

### 4. Arch User Repository (AUR)
Arch Linux ha un sacco di software nei suoi repository. L'AUR estende il software offerto da Arch Linux. Dall'AUR si può ottenere una quantità industriale di software in Arch Linux.

AUR è il punto d'accesso per la comunità per fornire nuove applicazioni. Si possono cercare e installare applicazioni con l'aiuto di un helper AUR.

### 5. Senso di realizzazione
Come menziona James Clear nel suo libro *Atomic Habits*, ***la mente umana ama le sfide, ma solo se si trovano nella zona di difficoltà ottimale***.

Ricordi la sensazione di quando hai installato la tua prima distro, anche se era solo Linux Mint? Ti ha lasciato un senso di soddisfazione. Hai installato Linux con successo!

Se è da un po' che usi Ubuntu o Fedora o un'altra distro e ti senti tuo agio (o annoiato), prova ad installare Arch Linux.

Per un utente Linux di media esperienza, installare Arch Linux dà un senso di realizzazione.

È una sfida, ma una sfida raggiungibile. Se si suggerisce a un nuovo utente di provare Arch Linux o addirittura cose più complicate come [Linux From Scratch](http://www.linuxfromscratch.org/), la sfida sarebbe troppo difficile da raggiungere.

Il senso di completare una sfida con successo è anche uno dei motivi per cui delle persone usano Arch Linux.

### 6. Nessuna azienda coinvolta! Arch è creato, supportato e posseduto dalla comunità
Ubuntu è supportato da Canonical, Fedora da Red Hat (che ora è parte di IBM) e openSUSE è di SUSE. Tutti queste distribuzioni importanti sono supportate da aziende.

Questo fatto non è la fine del mondo. Ma ci sono persone a cui non piace il fatto di avere aziende immischiate in progetti open-source.

Arch Linux, al pari di Debian, è uno dei rari progetti portati avanti esclusivamente dalla comunità.

Qualcuno potrebbe evidenziare che varie altre distribuzioni come Linux Mint non sono sponsorizzate da un'azienda. Questo può essere vero, ma Linux Mint è basato su Ubuntu e usa i repository Ubuntu. Arch Linux non deriva da nessun altra distro.

In questo senso, [Debian](https://www.debian.org/) e Arch Linux sono progetti trainati dalla comunità "più puri". Questo fatto può essere indifferente per molti, ma ad alcune persone interessano dettagli di questo tipo.

{{itsfoss(da="https://itsfoss.com/why-arch-linux/", titolo="Why People Are Crazy About Arch Linux? What’s so Special About it?")}}
