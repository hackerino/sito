+++
title = "nnn: un gestore file per chi non si accontenta"
date = 2020-01-25
[taxonomies]
tags = [ "terminale", "gestore file"]
categorie = [ "Software" ]
+++

Oggi vi parlo di [nnn](https://github.com/jarun/nnn/), un **gestore file** estremamente veloce che, con un minimo sforzo iniziale, apporta grandi vantaggi in produttività. È **scritto in C** ed è disponibile per svariati gusti di **Linux** oltre che per **MacOS**, **BSD** e altri sistemi di tipo UNIX.

## Funzionalità di nnn

**nnn** non fa rimpiangere nessuna funzionalità di un comune gestore di file grafico. Un elenco non esaustivo comprende:

* **navigazione** tramite frecce o tasti Vim
* **apertura dei file** tramite xdg-open (in Linux) o open (in MacOS)
* **creazione** file, cartelle e collegamenti
* **selezione** singola o per *range*
* **rinomina singola** e **multipla** (i nomi dei file sono presentati uno per riga in un *buffer* Vim e al salvataggio rinominati)
* **copia** e **spostamento**
* mostra/nascondi **file nascosti**
* mostra/nascondi **dettagli**
* **cd all’uscita** nell’ultima cartella (richiede configurazione)
* connessione e navigazione di **file remoti** (richiede sshfs o rclone)

Importante la disponibilità di un **cheatsheet** a portata di mignolo (consultabile con ?), che risparmia di ricordarsi tutte le combinazioni a memoria (e sono davvero tante).

Sono supportati inoltre i **plugin**, che sono implementati mediante script esterni, con la libertà che possono essere scritti in ogni linguaggio. Alcuni [esempi](https://github.com/jarun/nnn/tree/master/plugins) sono presenti nel *repository* GitHub.

## Configurazione minima consigliata

Sebbene **nnn** sia funzionale anche senza configurazione, è consigliabile mettere a punto alcuni dettagli.

Il più importante è indubbiamente il **cd all’uscita**, che di base non è attivo perché dipende dalla *shell* utilizzata. Per configurarlo si deve copiare il contenuto del file relativo al proprio interprete da [questo link](https://github.com/jarun/nnn/tree/master/misc/quitcd) e appenderlo al proprio file rc (presente nella cartella `$HOME` come .[shell]rc (ad esempio .bashrc)). A questo punto bisogna riavviare la shell (per fare in modo che il file rc venga riletto) e lanciare nnn con il **nuovo nome** appena definito (**n** se non modificato).

Nello stesso file è consigliabile definire, se non sono già presenti, le variabili `VISUAL` (o `EDITOR`) e `PAGER` con il nome del programma preferito per la modifica e la paginazione (preferibilmente CLI). Ad esempio:

```
export EDITOR=pico
export PAGER=more
```

Un’altra variabile disponibile utile soprattutto per l’integrazione con i *desktop environment* è `NNN_TRASH`, che se impostata sfrutta il comando **trash-cli** al posto di **rm**, cestinando dunque i file invece di eliminarli (deve essere disponibile sul vostro sistema). I file rimossi da nnn saranno a questo punto visibili anche nel cestino del vostro gestore file grafico.

```
export NNN_TRASH=1
```

## Alcuni casi d’uso di nnn

nnn può essere la scelta giusta come gestore file in diverse circostanze.

Prima di tutto la possibilità che offre di connettersi tramite **SSH** è vitale per chi come me gestisce un sito o a un sistemista che opera su un server a distanza.

Ha un’**interfaccia più intuitiva** rispetto al classico `comando -opzione` che può intimidire un nabbo, per questo può essere il **primo approccio** al terminale di un cliccatore seriale. In più permette anche di passare comandi direttamente alla **shell**, così da poter sperimentare e imparare senza fare danni.

La possibilità di fare il cosiddetto *cherry picking* con spazio (in pratica una selezione come con CTRL+click) è poi fondamentale quando si devono copiare o spostare file che non hanno nulla in comune nel nome, evitando di doverli scrivere tutti.

Insomma sta a voi spulciare tra le varie opzioni e trovare quella più adatta al vostro flusso di lavoro. Per approfondire è d’obbligo un’occhiata alla pagina del **manuale**, ricca e concisa, oppure alla [wiki](https://github.com/jarun/nnn/wiki) che è invece più diluita nei contenuti.
