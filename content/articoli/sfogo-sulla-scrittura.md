+++
title = "Sfogo sulla scrittura"
date = 2020-09-17
[taxonomies]
tags = [ "blog", "internet", "scrittura"]
categorie = [ "Riflessioni" ]
+++

La concisione è un'arte, spesso però trovo che chi scrive abbia tutta l'intenzione di sprecare il tempo di chi legge, oltre che il suo, per diffondere parole vuote.

Le parole del prof. Eco rimbombano: "i social media danno diritto di parola a legioni di imbecilli". La soglia di sbarramento per condividere le proprie idee si è praticamente azzerata e dopo cinque anni dalla pronuncia della citata frase non sono che aumentati i dubbi su come uno dovrebbe scegliere i contenuti da fruire. Perché nessuno ha tempo infinito, ma se prima la difficoltà di mettere in piedi un sito o di pubblicare un libro in modo indipendente alzava la qualità media, adesso si è obbligati a fare selezione. Dovremmo forse affidarci a siti di recensioni, che però hanno lo stesso problema dei contenuti, ovvero possono che essere curati da chiunque. Penso a siti come GoodReads, dove un libro come "Padre ricco, padre povero", che è stato essenzialmente smontato punto per punto da [John T. Reed](https://johntreed.com/blogs/john-t-reed-s-real-estate-investment-blog/61651011-john-t-reeds-analysis-of-robert-t-kiyosakis-book-rich-dad-poor-dad-part-1), continua a macinare voti positivi a causa delle scarse conoscenze del pubblico che non ha competenze per valutarlo.

Forse dovremmo ignorare ogni libro uscito negli ultimi anni finché non viene recensito da qualcuno di competente, sacrificando così l'effetto positivo della rapida condivisione della conoscenza. E anche così facendo dubito però che esista un sistema simile per le varie pagine social o blog, dove ognuno è libero di straparlare e alimentare fake news e complottismi vari.

Non lo so, questo è uno sfogo che cade nel vuoto, non vedo soluzioni. Sfrutto almeno il momento per lanciare un appello a chi scrive: per favore sentitevi responsabili del tempo perso da chi legge, e non scrivete se non avete nulla da dire. Grazie.
