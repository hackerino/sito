+++
title = "Linux Journal è tornato"
date = 2020-10-01
[taxonomies]
tags = [ "linux", "notizie"]
categorie = [ "Linux" ]
+++

Microscopico articolo per comunicare anche a voi che *Linux Journal* è risorto, per l'ennesima volta. Io come al solito arrivo in ritardo, lo so, ma è una notizia che in Italia non ho praticamente visto circolare.

Nel [comunicato ufficiale](https://www.linuxjournal.com/content/linux-journal-back) infatti viene spiegato come il nome e l'attività sono stati acquisiti da Slashdot Media, che già opera nel mondo open-source con altri due progetti storici dell'ambiente, appunto *Slashdot* e *Sourceforge*.

Inutile sottolineare l'importanza che ha per l'ambiente un sito di questo livello, il quale porta con sé 25 anni di fama e raccoglie alcuni tra i migliori articoli a tema Linux, con contributori eccellenti tra i quali spiccano *Bryan Lunduke* e *Eric S. Raymonds*. Anche solo per preservare questo genere di materiale sarebbe stato importante mantenere il sito on-line. Ora che qualcuno paga le bollette si possono dormire sonni più tranquilli.
