+++
title = "I corsi online sono nauseanti"
date = 2020-05-16

[taxonomies]
tags = ["vita"]
categorie = ["Riflessioni"]
+++

Sono stufo marcio di vedere corsi in vendita a prezzi completamente assurdi, che viaggiano sul confine tra la truffa e la pessima qualità-prezzo.

Sono ancora più stanco e in parte dispiaciuto di vedere persone o così ingenue o così testarde da non rendersi conto della realtà e che giurano che quel corso li ha cambiati e gli è servito e garantiscono a nome del corso, quasi siano motivati da un qualche tipo di ideologia.

Sono utente di Internet da abbastanza anni da aver allenato la mia intelligenza artificiale biologica nel riconoscere questo tipo di situazioni, e sicuramente mi ha aiutato anche la mia tirchieria da buon piemontese che aiuta quotidianamente a rallentare ogni impulso.

Ma gli utenti di internet non sono tutti come me, e le manipolazioni psicologiche diventano sempre più sottili e personalizzate al punto che non solo gli anziani sono esposti (anche perché difficilmente comprerebbero un corso online). Ora non intendo dire che io sono il miglior annusatruffe, potrebbero anche ingannare me e probabilmente capiterà, ma la questione è che la maggioranza degli utenti sono più deboli di me e sono molto più soggetti a questi tipi di manipolazioni.

Il discorso è quindi il seguente: se tutti i corsi fossero gratuiti, tutti apporterebbero un qualche tipo di valore, e la spesa è “soltanto” in termini di tempo, a meno che non vengano raccontate soltanto balle. Dato che invece per i corsi ci vogliono dei soldi si perde due volte: in termini di tempo e in termini di tempo per guadagnarti quei soldi. Bell’affare!

Le testimonianze in bella mostra sul sito stesso hanno poi una valenza pari a (l’avete indovinato) zero. Se non esiste una mediazione potrei tranquillamente pagare la mia bella truppa di indiani a scrivermi le recensioni a colpi di Google Translate. Bella roba. E non pensate che invece su siti terzi sia troppo meglio: sono numerosi gli esempi di locali che ti obbligano e ti guardano mettere cinque stelle su siti come TripAdvisor e parenti.

Insomma, viviamo in un mondo difficile. Siamo davvero convinti che se una maggioranza dice che X funziona, funziona davvero? Perché dovreste fidarvi delle dita mosse da uno sfogo? Farsi truffare, non è anche questa la libertà?
