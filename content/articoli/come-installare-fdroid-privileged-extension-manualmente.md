+++
title = "Come installare F-Droid Privileged Extension (e altre app di sistema priv-app)"
date = 2022-02-18
[taxonomies]
tags = [ "android", "fdroid"]
categorie = [ "Android" ]
+++

Come probabilmente saprete se state leggendo questo articolo, [F-Droid Privileged Extension](https://f-droid.org/it/packages/org.fdroid.fdroid.privileged/) (estensione privilegiata in italiano) è un'app di sistema il cui scopo principale è quello di permettere di installare e aggiornare automaticamente le applicazioni di [F-Droid](https://f-droid.org). Il problema però è che per installare questa componente di F-Droid è necessario "flashare" uno zip da recovery, ma non ciò funziona da tempo sulle versioni più recenti di Android. 

Vediamo dunque come installare manualmente un'applicazione come `priv-app`, seguendo questo esempio specifico.

### Nota bene
Descriverò il procedimento che ha funzionato per il mio dispositivo, visto che mi pare di aver letto almeno un'altra procedura simile che sembra funzionare per qualcuno. Il mio smartphone è un **Poco X3 Pro** con **Lineage OS 18.1**.

In ogni caso non si faranno danni, perché la copia dei file sarebbe proprio impossibile, quindi tentar non nuoce.

## Prerequisiti
È necessario avere i permessi di root (Magisk) e il debug USB, ed aver acconsentito all'uso della shell con permessi root in Magisk.

Potrebbe essere necessario attivare nelle opzioni per sviluppatori l'opzione "Debug con accesso root", ma nel mio caso non è stato necessario. 

## Procedimento
Scaricate lo [ZIP](https://f-droid.org/it/packages/org.fdroid.fdroid.privileged.ota/) dal sito ed estraetelo. Troverete al suo interno qualche file, tra cui:

* due file `F-Droid\*.apk`, che saranno da copiare in `/system/priv-app`
* un file `permissions_org.fdroid.fdroid.privileged.xml`, che sarà da copiare in `/etc/permissions`

Problema: la partizione `/system` è in sola lettura. Che fare? Nel mio caso il procedimento è stato:

```sh
adb shell
su
remount
```
A questo punto abbiamo ri-montato tutte le partizioni in lettura-scrittura. Questo è il punto di possibile fallimento, l'errore più probabile credo possa essere `command not found`. In questo caso potete provare a seguire il procedimento alternativo che potete trovare nelle conclusioni.

Se il comando non dice nulla, possiamo procedere. Supponendo di avere già copiato i tre file dal PC alla radice della memoria interna (che si trova in `/sdcard`):

```sh
mv /sdcard/F-Droid*.apk /system/priv-app
mv /sdcard/permissions_org.fdroid.fdroid.privileged.xml /etc/permissions
chmod 644 /system/priv-app/F-Droid*.apk
chmod 644 /etc/permissions/permissions_org.fdroid.fdroid.privileged.xml
chown root /system/priv-app/F-Droid*.apk
chown root /etc/permissions/permissions_org.fdroid.fdroid.privileged.xml
reboot
```

A parole: abbiamo copiato i file, cambiato i permessi e l'utente proprietario, e riavviato. Dopo il riavvio troveremo l'app installata.

A questo punto si può installare anche da F-Droid l'omonima app che permette di aggiornare la Privileged Extension, e che farà comparire un'opzione nelle impostazioni, l'ultima in fondo. Fatto questo, le app si potranno installare e aggiornare da sole (se attivate questa opzione), come fa il Play Store. 

## Conclusione
Un procedimento un po' contorto e che potrebbe spaventare qualcuno, ma ne vale assolutamnte la pena visto il risultato. Spero anch'io che in futuro tutto ciò non sarà più necessario, ma per il momento questa è la soluzione. Lascio anche un [procedimento alternativo](https://lynxbee.com/how-to-install-and-uninstall-android-application-as-system-app-priv-app/), che per me personalmente non ha funzionato ma potrebbe tornare utile a qualcun altro.

Consiglio inoltre di non fissarsi sull'esempio, ma di notare che si può applicare per esempio allo stesso Play Store, o ad altre applicazioni che hanno bisogno di essere installate come priv-app per questioni di permessi.

Grazie per l'attenzione, a risentirci.
