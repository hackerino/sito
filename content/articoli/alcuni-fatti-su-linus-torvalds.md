+++
title = "Alcuni aneddoti su Linus Torvalds"
date = 2020-10-08
[taxonomies]
tags = [ "linux", "linus torvalds"]
categorie = [ "Linux" ]
+++

*In breve: alcuni sono noti, altri un po' meno, ecco 20 aneddoti su Linus Torvalds, il creatore del kernel Linux.*

[Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds), uno studente finlandese, sviluppò un sistema operativo simil-Unix durante il suo master nel 1991. Da allora ha scatenato una rivoluzione: oggi è alla base della maggior parte del web, di molti dispositivi embedded e di tutti i [primi 500 supercomputer](https://itsfoss.com/linux-runs-top-supercomputers/).

## 1. Il suo nome viene da un premio Nobel
Linus Benedict Torvalds è nato il 28 dicembre 1969 a Helsinki. Discende da una famiglia di giornalisti. Suo padre è [Nils Torvalds](https://en.wikipedia.org/wiki/Nils_Torvalds), un politico finlandese ed europarlamentare.

È stato chiamato così in onore di Linus Pauling, doppio premio Nobel per la chimica e la pace.

## 2. Tutti i Torvalds nel mondo sono parenti
Anche se si possono trovare diverse persone con il nome Linus, non troverete mai molte persone col cognome Torvalds - perché l'ortografia "corretta" sarebbe in realtà Torvald (senza la s). Suo nonno ha cambiato il suo nome da Torvald a Torvalds, aggiungendoci una "s" alla fine. E così iniziò la dinastia dei Torvalds (se si può chiamare così).

Dato che è un cognome così insolito, esistono appena trenta Torvalds al mondo e sono tutti parenti, secondo ciò che sostiene Linus Torvalds nella sua biografia.

## 3. Il Commodore Vic 20 è stato il suo primo computer
All'età di 10 anni Linus ha iniziato a scrivere programmi in BASIC, sul Commodore Vic 20 del nonno materno. Fu allora che scoprì il suo amore per i computer e la programmazione.

## 4. Il sottotenente Linus Torvalds
Sebbene preferisse passare il tempo al computer piuttosto che in attività atletiche, ha dovuto frequentare gli allenamenti militari obbligatori. Aveva il grado di Sottotenente.

## 5. Creò Linux perché non aveva soldi per UNIX
All'inizio del 1991, scontento di [MS-DOS](https://en.wikipedia.org/wiki/MS-DOS) e [MINIX](https://www.minix3.org/), Torvalds voleva comprare un sistema UNIX. Fortunatamente per noi non aveva abbastanza soldi. Così decise di creare il suo clone di UNIX, da zero.

## 6. Linux avrebbe potuto chiamarsi Freax
Nel settembre del '91, Linus annunciò Linux (acronimo di 'Linus's MINIX') e incoraggiò i suoi colleghi ad usare il suo codice sorgente per incoraggiarne la distribuzione.

Linus pensava che il nome Linux fosse troppo egoista. Voleva cambiarlo in Freax (basato su free, freak e MINIX), ma il suo amico Lemmarke aveva già creato una directory chiamata Linux sul suo server FTP. E così il nome Linux sopravvisse.

## 7. Linux era il suo progetto principale all'università
"Linux: un sistema operativo portabile" era il titolo della sua tesi per il suo M.Sc.

## 8. Ha sposato una sua studentessa
Nel 1993, quando insegnava all'Università di Helsinki, diede il compito di comporre e-mail come compito a casa ai suoi studenti. Sì, comporre le email era importante a quei tempi.

Una studentessa di nome Tove Monni completò il compito inviandogli un'e-mail in cui gli chiedeva un appuntamento. Lui accettò, e tre anni dopo nacque la prima delle loro tre figlie.

## 9. Linus ha un asteroide che porta il suo nome
Il suo nome è stato insignito di numerosi premi, tra cui un asteroide chiamato [9793 Torvalds](http://enacademic.com/dic.nsf/enwiki/1928421).

## 10. Linus ha dovuto lottare per il marchio di Linux
Linux è un marchio registrato da Linus Torvalds. Torvalds non si preoccupò inizialmente del marchio, ma nell'agosto del 1994 un certo William R. Della Croce Jr. registrò il marchio Linux e iniziò a chiedere royalties agli sviluppatori di Linux. Torvalds in paga gli fece causa e nel 1997 il caso fu risolto.

## 11. Steve Jobs voleva che lavorasse sul MacOS di Apple
Nel 2000, il fondatore di Apple, Steve Jobs, [lo invitò a lavorare su macOS di Apple](https://www.macrumors.com/2012/03/22/steve-jobs-tried-to-hire-linux-creator-linus-torvalds-to-work-on-os-x/). Linus rifiutò la lucrosa offerta e continuò a lavorare sul kernel Linux.

## 12. Linus ha creato anche Git
La maggior parte delle persone conosce Linus Torvalds per la creazione del kernel Linux. Ma ha anche creato [Git](https://en.wikipedia.org/wiki/Git), un sistema di versionamento che è ampiamente utilizzato nello sviluppo di software in tutto il mondo.

Fino al 2005, il servizio (al tempo) proprietario [BitKeeper](https://www.bitkeeper.org/) era usato per lo sviluppo del kernel Linux. Quando Bitkeeper chiuse il suo servizio gratuito, Linus Torvalds creò Git da sé, perché nessuno degli altri sistemi di versionamento soddisfaceva le sue esigenze.

## 13. Linus non programma quasi più
Anche se Linus lavora a tempo pieno sul kernel Linux, non ci scrive quasi più codice. Di fatto la maggior parte del codice del kernel Linux è scritto da collaboratori di tutto il mondo. Egli si assicura che le cose vadano bene ad ogni rilascio, con l'aiuto dei manutentori del kernel.

## 14. Torvalds odia il C++
Linus Torvalds ha [una forte avversione per il linguaggio di programmazione C++](https://lwn.net/Articles/249460/). Ne ha parlato molto apertamente. Scherza sul fatto che il kernel di Linux compila più velocemente di un programma C++.

## 15. Anche Linus Torvalds ha trovato Linux difficile da installare (ora potete sentirvi bene con voi stessi)
Qualche anno fa, Linus disse di [aver trovato Debian difficile da installare](https://www.youtube.com/watch?v=qHGTs1NSB1s). [È noto che usa Fedora](https://plus.google.com/+LinusTorvalds/posts/Wh3qTjMMbLC) sulla sua postazione di lavoro principale.

## 16. Adora le immersioni subacquee
Linus Torvalds ama le immersioni subacquee. Ha persino creato [Subsurface](https://subsurface-divelog.org/), uno strumento per i subacquei per tenere un registro di immersioni. Rimarrete sorpresi dal fatto che sul suo forum a volte risponde anche a domande di carattere generale.

## 17. Il Torvalds volgare ha migliorato il suo comportamento
Torvalds è noto per l'uso di [blande imprecazioni](https://www.theregister.co.uk/2016/08/26/linus_torvalds_calls_own_lawyers_nasty_festering_disease/) nella mailing list del kernel Linux. Questo è stato criticato da alcuni nell'industria. Tuttavia, risulta difficile criticare la sua uscita "[F\*\*k you, NVIDIA](https://www.youtube.com/watch?v=_36yNWw_07g)", in richiesta di un miglior supporto per il kernel Linux da parte di NVIDIA.

Nel 2018, [Torvalds si prese una pausa dallo sviluppo del kernel Linux per migliorare il suo comportamento](https://itsfoss.com/torvalds-takes-a-break-from-linux/). Questo accadde poco prima di firmare il controverso [codice di condotta per gli sviluppatori del kernel Linux](https://itsfoss.com/linux-code-of-conduct/).

## 18. È troppo timido per parlare in pubblico
Linus non si sente a suo agio a parlare in pubblico. Non partecipa a molti eventi. E quando lo fa, preferisce sedersi ed essere intervistato dall'ospite. Questo è il suo modo preferito di fare un discorso pubblico.

## 19. Non è un fanatico di social media
[Google Plus](https://plus.google.com/+LinusTorvalds) è l'unica piattaforma di social media che ha usato. Ha anche impiegato un po' di tempo libero per [recensire dei gadget](https://plus.google.com/collection/4lfbIE). Google Plus ora è morto, quindi non ha altri account sui social media.

## 20. Torvalds si è stabilito negli USA
Linus si è trasferito negli Stati Uniti nel 1997 e vi si è stabilito con la moglie Tove e le loro tre figlie. È diventato cittadino americano nel 2010. Attualmente, lavora a tempo pieno sul kernel Linux come parte della [Linux Foundation](https://www.linuxfoundation.org/).

È difficile dire quale sia il patrimonio netto di Linus Torvalds o quanto guadagni perché queste informazioni non sono mai state rese pubbliche.

Se siete interessati a saperne di più sui primi anni di vita di Linus Torvalds, vi consiglio di leggere la sua biografia intitolata [Just for Fun](https://amzn.to/3npbxc2).

{{itsfoss(da="https://itsfoss.com/linus-torvalds-facts/", titolo="Linus Torvalds: 20 Facts About the Creator of Linux")}}
