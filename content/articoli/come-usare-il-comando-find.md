+++
title = "Come usare il comando find"
date = 2020-12-29
[taxonomies]
tags = [ "terminale", "linux"]
categorie = [ "Software" ]
+++

Il comando **find** è uno dei comandi standard di UNIX e permette di eseguire operazioni di ricerca e manutenzione su un insieme di file specificato. In questo articolo ne sarà illustrata la logica oltre ad alcuni casi d'uso tipici. Iniziamo.

## La logica del comando
Il comando find è usato essenzialmente per elencare tutti i file di una cartella ricorsivamente (quindi anche nelle sottocartelle).

È poi possibile filtrare solo quelli che rispettano certi requisiti, specificati come opzioni del comando. Tra questi requisiti i più comuni sono: il nome del file, il percorso, il tipo del file (se è un file regolare, un direttorio, un collegamento...) e il massimo o minimo livello di profondità, ma ne esistono molti altri meno comuni (vedi *man*).
Inoltre è possibile abbinare vari requisiti per mezzo degli operatori booleani `-a`, `-o` e `!`, rispettivamente *and*, *or* e *not*, per cui è possibile specificare la ricerca in modo ancora più dettagliato.

La seconda parte potente del comando è la possibilità di eseguire un certo comando su ogni elemento trovato dalla ricerca con `-exec comando {} \;`, quindi ad esempio si può chiamare il comando `rm -f`, oppure usare grep per cercare una certa stringa voluta in ogni file. Il simbolo `{}` rappresenta uno tra tutti i nomi di file validi per find sui quali sarà eseguito il comando, un file alla volta. Il bello di questo comando è che è d'uso generale e si presta a molti usi, il limite come al solito è la fantasia.

In genere il comando find è un po' più robusto per quanto riguarda la scrittura di script (scriptura? scriptatura?), per cui potreste trovarlo in posti dove un semplice `ls` potrebbe funzionare ugualmente bene. Questo problema è stato trattato nel dettaglio in questo ottimo [articolo di Saverio Miroddi](https://saveriomiroddi.github.io/Files-listing-experiments-find-vs-ls/). 

## Esempi tipici e idee
Vediamo ora qualche esempio. Dato che lo spazio sul pianeta terra è limitato, non troverete l'esatto comando che fa al caso vostro. Provate a immaginare un caso in cui il comando può esservi utile, e nel caso adattarlo.  

### Elencare i file con un certo filtro 
Elenca tutti i file esistenti nel sottoalbero corrente.
```sh
$ find .
```

Lo stesso ma soltanto per i file regolari. Utile ad esempio negli script.
```sh
$ find . -type f
```

Ancora lo stesso ma soltanto per i direttori. 
```sh
$ find . -type d
```

Praticamente equivalente a `ls`, usato a volte negli script.
```sh
$ find . -maxdepth 1
```

Cerca tutti i file con un dato nome. Si può cambiare `-name` con `-iname` per una ricerca insensibile alle maiuscole. 
```sh
$ find . -name "pippo.txt"
```

### Eseguire dei comandi sui file trovati
Rimuove tutti i file che riscontrano un pattern nel nome. In questo esempio rappresenta tutti i file `.txt`.
```sh
$ find . -name "*.txt" -exec rm -f {} \;
```

Greppa tutti i documenti cercando la parola chiave main. Utile nel restringere il campo e scoprire dove si trova il main in un repo malorganizzato. Un altro caso utile potrebbe essere quando serve cercare da dove proviene un certo messaggio di errore o una certa stringa. Fondamentale l'opzione `-H`, che forza grep a stampare il file di provenienza.
```sh
$ find . -name "*.c" -exec grep -H main {} \;
```
