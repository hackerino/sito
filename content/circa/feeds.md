+++
title = "Tutti i feed"
path = "feeds"
date = 2020-10-25
template = "about.html"
+++

Questo è l'elenco dei feed disponibili per abbonarsi al sito. Sono in formato Atom.

* [Articoli](/articoli/atom.xml)
* [Recensioni](/recensioni/atom.xml)

Ci si può anche iscrivere ai feed singoli di categoria o tag per gli articoli, e tipo di contenuto per le recensioni. Lo si può fare dalla lista dell'elemento di interesse, cliccando sul link "feed" a fianco del titolo.
