+++
template = "about.html"
+++
# Filosofia

Questo è un sito atipico, ma è la bellezza di avere un sito proprio e non un semplice account su \*inserire social network\*: libertà assoluta. 

Le regole principali della mia filosofia sono qualcosa del genere, ma sono estremamente mutevoli:

* usa solo roba utile (no JS, font esterni o immagini solo per abbellire)
* sfrutta pesantemente i link ipertestuali per collegare le informazioni 
* quando scrivo in una lingua, scrivo in quella lingua, tranne dove viene ostacolata la comprensione (non mi metto a tradurre "computer" e simili) 
