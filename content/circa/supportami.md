+++
title = "Contribuisci"
path = "contribuisci"
date = 2020-10-25
template = "about.html"
+++

Ci sono vari modi per contribuire a questo sito se avete voglia di farlo, elencati dal più semplice al più dispendioso.

## Fammi sapere cosa ne pensi

Il modo più semplice di contribuire è quello di farmi sapere cosa ne pensi di ciò che scrivo. 

Si può fare tramite la seguente e-mail: **simone at nigge dot rs**.

In futuro aggiungerò ulteriori sistemi (magari un canale Matrix o un sistema di commenti anonimo, non so), ma per il momento questo è il modo migliore che conosco.

## Correzione di refusi

Se vedete un errore di stampa e la cosa vi infastidisce, oppure notate che il CSS è completamente sballato sul vostro dispositivo, buone notizie: **questo sito è interamente open-source** e disponibile su [Gitlab](https://gitlab.com/hackerino/sito), per cui se avete tempo e voglia potete contribuire **clonando il repo** e mandando una **merge request**. Il sito è stato generato usando il generatore di siti statici Zola, per cui l'unica dipendenza per lavorare e quella. Ogni contributo è ben accetto.

## Donazioni

Per quanto internet abbia reso possibile comunicare a bassissimo costo, ci sono comunque delle spese da sostenere per tenere in piedi la baracca.

Se avete trovato utile questo sito e volete contribuire, queste sono alcune opzioni per supportarmi; ogni centesimo è apprezzato.

### Opzioni
**PayPal** (non conosco un sistema migliore):
<https://paypal.me/dottolinux>

**Bitcoin**:
3552mwBunGCXuCW161eqjHad2s4xjVNfus
