+++
template = "about.html"
+++

# Cos'è sta roba

Benvenuti ne "La discarica di Dotto", un sito che conserva il fascino di recuperare roba utile in mezzo a montagne di spazzatura. Troverete di tutto e troppo sugli argomenti che mi interessano, e nulla sul resto. 

Dovrebbe assomigliare grossomodo a un sistema di raccolta sistematica di informazioni, ispirato dallo Zettelkasten e sistemi simili, scritte in italiano perché mi viene naturale (sarebbe meglio in inglese o esperanto, ma sposterei troppo il focus sulla scrittura).

Diciamo che mi capisco da solo, non garantisco nulla per gli altri, ma può essere comunque interessante. 
