+++
template = "about.html"
+++
# Chi sono
Mi chiamo Simone Dotto, sono nato agli sgoccioli del millennio e sono un essere umano qualsiasi, mediamente stupido. 

Mi appassiona tutto ciò che mi permette di capire come funziona il mondo, per cui tutto ciò che non è anti-scientifico in sostanza. Nello specifico, in questa prima fase della mia vita mi è capitato di approfondire l'informatica, e ciò mi ha avvicinato al mondo del software libero, copyleft e compagnia bella. Per questo motivo i contenuti del sito sono rilasciati con licenza [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).
