+++
title = "SCoT"
date = 2022-02-21
[taxonomies]
tags = [ "sts", "sociologia"]
categorie = [ "Appunti" ]
+++

Un approccio allo studio dell'evoluzione di una tecnologia che si concentra sui gruppi sociali e sui problemi percepiti da ognuno dei gruppi interessati. È contrario all'idea che la tecnologia migliore sopravviva, ma invece sostiene che a sopravvivere sia un compromesso tra gli interessi di tutti i gruppi sociali interessati. 
