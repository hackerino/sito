+++
title = "Pratica sociale"
date = 2022-02-21
[taxonomies]
tags = [ "sts", "sociologia"]
categorie = [ "Appunti" ]
+++

Un comportamento diffuso e non autoritario che è condiviso da un gruppo di individui, e che non può essere tale senza la contemporanea partecipazione di altri membri.

Si compone di tre elementi:

1. il materiale, ovvero ciò di concreto che serve per la pratica
2. la competenza, ovvero tutto ciò che è necessario conoscere per portare avanti la pratica
3. il significato, ovvero la motivazione che spinge a perseguire la pratica
